function [str] = mat2str(mat)
  s = size(mat);
  if (s(1)==1)&(s(2)==1)
    str = string(mat);
  else
    str = "[";
    for i=1:s(1)
      if i > 1
        str = str + ";";
      end
      for j=1:s(2)
        if j > 1
          str = str + ",";
        end
        str = str + string(mat(i,j));
      end
    end
    str = str + "]";
  end
endfunction
