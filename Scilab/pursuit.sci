	//As, B et C DOIVENT être correctes
	//lambdas doit être une matrice ligne

	// si pour tout k dans [|0,n-2|] C*As^(k)*B = 0
	// Upn  =  -[(C*As^(n)+Somme[i=0:n-1](lambda(i)*C*As^(i))/(C*As^(n-1)*B)]*Z
	//	   +[(yd(n)+Somme[i=0:n-1](lambda(i)*yd(i))/(C*As^(n-1)*B)]
	//	=  L*Z + Somme[i=0:n](Mi*yd(n=i)

function [err,L,Mi] = pursuit(As,B,C,lambdas)		// As= A+BK (K:Ackerman)

		//vérification des matrices
	[ordre,ne,ns] = check_mat(As,B,C);

		//remplissage des matrices B et C
	[Bf,Cf] = fill_mat(B,C,ne,ns,ordre);

		//calcul de l'ordre de dérivée à asservir
	n = compute_n(As,B,C);

		//err=-1 et sortie de la fonction si Cf*As^(n)*Bf n'est pas inversible
	err = 0;
	if (det(Cf*As^(n)*Bf) == 0) then
		err = -1;
		return;
	end

		//calcul de Up(n)
	K = (C*As^(n-1)*B)^(-1);
	K2 = C;

	L = -K*(C*As^(n));
	for i=0:n
		L = L-K*lambdas[i+1]*K2;
		K2 = K2*As;
	end

	Mi = K;
	for i=n-1:-1:0
		Mi = [K*lambdas[i+1] Mi];
	end

endfunction

