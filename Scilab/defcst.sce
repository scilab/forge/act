//*  defcst.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Définit les constantes utiles pour les scripts.
//


// CST_I_H
//
//	L'entrée est une fonction de tranfère.

CST_I_H = 5;

// CST_I_POLE
//
//	Le système est définit par les pôles de la fonction de transfère.
CST_I_POLE = 2;

// CST_I_ALPHA
//
//	Le système est définit par les coeficients du polynôme caractéristique.
CST_I_ALPHA = 3;

// CST_I_EQU
//
//	Le système est définit par un système d'équation.
CST_I_EQU = 4;

// CST_I_MAT
//
//	Le système est définit par des matrices.
CST_I_MAT = 1;

// ---------------

// CST_SM_ACKER
//
//	On utilise méthode Akerman pour la stabilisation
CST_SM_ACKER = 2;

// CST_SM_ACKER
//
//	On utilise méthode Brunovsky pour la stabilisation
CST_SM_BRU = 3;

// CST_SM_ACKER
//
//	On utilise méthode ppol pour la stabilisation
CST_SM_PPOL = 1;

// --------------

// CST_PM_DEFAULT
//
//	On utilise la méthode par défaut pour la poursuite.
CST_PM_DEFAUTL = 1;


// ------------

CST_SIM_STEP = 1;

CST_SIM_DIRAC = 2;

CST_SIM_RAMP = 3;


i = %i;















