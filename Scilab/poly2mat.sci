//*  poly2mat.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Calcul les matrices A, B et C du système linéaire à partir du dénominateur
// de la fonction de transfère (en s).
//	Le dénominateur doit être de la forme den = s^n + a_{n-1} s^{n-1} ... + a_1 s + a_0
//
//*  Argument :
//
//	den : dénominateur de la fonction de transfert.
//
//*  Retours :
//
//	* A : matrice de dynamique (n*n)
//	* B : matrice de commande (n*1)
//	* C : matrice d'observation (1*n)
//
//*  Avertissement :
//
//	* Le coefficient associé à s^n (du dénominateur) DOIT être égal à 1.
//

function [A,B,C] = poly2mat(den)

	c = coeff(den);

	n = length(c)-1;

	A = [[zeros(n-1,1),eye(n-1,n-1)];zeros(1,n)];
	B = zeros(n,1);
	B(n) = 1;
	C = zeros(1,n);
	C(n) = 1;

	for i = 1:n
		A(n,i) = -c(i);
	end

endfunction


