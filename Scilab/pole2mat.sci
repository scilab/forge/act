//*  pole2mat.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Calcul les matrices A, B et C du système linéaire à partir des poles du
// dénominateur de la fonction de transfère (en s).
//	Le dénominateur doit être de la forme den = s^n + a_{n-1} s^{n-1} ... + a_1 s + a_0
//
//*  Argument :
//
//	pole : pôles du dénominateur de la fonction de transfère.
//
//*  Retours :
//
//	* A : matrice de dynamique (n*n)
//	* B : matrice de commande (n*1)
//	* C : matrice d'observation (1*n)
//
//*  Avertissement :
//
//	* pole DOIT être de dimension correcte. (n)
//
//* Déclaration nécessaire :
//
//	* poly2mat.sci
//

function [A,B,C] = pole2mat(pole)

	s = poly(0,'s');
	p = 1;
	n = length(pole);

	for i = 1:n
		p = p*(s-pole(i))
	end

	[A,B,C] = poly2mat(p);

endfunction


