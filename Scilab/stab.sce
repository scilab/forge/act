//*  stab.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Script de stabilisation.
//

g = ascii(34);
f = "Scilab/script_stab.sce";
e = execstr("exec(" + g + f + g + ");", "errcatch");
fd_w = file("open","dataSout.end", "new");
fprintf("it works !");
file("close",fd_w);
quit;
