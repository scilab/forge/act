//*  sys2mat.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Script de stabilisation.
//

// Récupération des définitions de constantes :
	
	exec("defcst.sce");

// Déclaration des fonctions

	exec("poly2mat.sci");
	exec("coef2mat.sci");
	exec("pole2mat.sci");

	exec("check_mat.sci");

	exec("mat2str.sci");

// Déclaration de la méthode d'ackerman

	exec("acker_coef.sci");
	exec("acker.sci");

// Conversion en matrice

	converr = 0;

	Di = [0.0];

	if exists("SystemInput") == 1 then
		if SystemInput == CST_I_H then
			// Vérification des éléments
			if H_c~='z' then
				[Ai,Bi,Ci] = poly2mat(H_den);
			elseif
				// On ne sais pas encore faire
				converr = -1;
			end

		elseif SystemInput == CST_I_MAT then
			Ai = matA;
			Bi = matB;
			Ci = matC;
			if exists("matD") == 1 then
				Di = matD;
			end

		elseif SystemInput == CST_I_POLE then
			[Ai,Bi,Ci] = pole2mat(poles);

		elseif SystemInput == CST_I_ALPHA then
			[Ai,Bi,Ci] = coef2mat(alpha);

		elseif SystemInput == CST_I_EQU then
			Ai = matA;
			Bi = matB;
			Ci = matC;
			if exists("matD") == 1 then
				Di = matD;
			end
		else
			// Erreur : Le type d'entrée donné n'existe pas.
			converr = 2
		end
	else
		// Erreur : Le fichier de donnée est erronnée (SystemInput inexistant).
		converr = 1
	end




