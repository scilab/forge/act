//*  acker.sci
//
//*  Authors :
//
//	Adrien KERFOURN
//	Alexandre ŒCONOMOS
//	Rémi THÉBAULT
//
//*  License :
//
//	CeCILL v2
//
//	See (in english) :
//
//		* Licence_CeCILL_V2-en.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
//
//	See (in french) :
//
//		* Licence_CeCILL_V2-fr.txt
//		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
//
//*  Description :
//
//	Méthode d'ackerman pour la stabilisation d'un système Mono-entrée / Mono-sortie
// (SISI).
//
//*  Arguments :
//
//	* A : Matrice de dynamique (n*n)
//	* B : Matrice de commande (n*1)
//	* p : Pôles désirés
//
//*  Retour :
//
//	* K : Commande de stabilisation.
//
//* Avertissement :
//
//	* A, B et p DOIVENT être de dimensions correctes.
//	* Le système DOIT être commandable (contrôlable).
//	* Le système ne DOIT contenir qu'une entrée et qu'une sortie (SISO).
//
//* Déclaration nécessaire :
//
//	* acker_coef.sci
//

function [K] = acker(A,B,p)

	n = length(p);

	s = poly(0,'s');
	equation = 1;

	for i = 1:n
		equation = equation*(s-p(i));
	end

	alpha = coeff(equation);
	
	K = acker_coef(A,B,alpha);

endfunction

