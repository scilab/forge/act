/*	ComplexException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Thrown to indicate a error when using a Complex object.
 */

package Complex;

/** Thrown to indicate a error when using a Complex object.
 */
public class ComplexException extends Exception
{
	/** Unknow error */
	public final static int E_UNKNOW = 0;
	/** [parse error] Expected number with the '*' symbol */
	public final static int E_PARSE_MUL = 1;
	/** [parse error] Incorrect number format */
	public final static int E_PARSE_VAL = 2;

	private int code;
	private String msg;

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 */
	public ComplexException(int code, String msg)
	{
		super();
		this.msg = msg;
		this.code = code;
	}

	/** Constructs a new exception without detail message.
	 */
	public ComplexException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public ComplexException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.msg = msg;
		this.code = code;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public ComplexException(Throwable cause)
	{
		this(0,"",cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case ComplexException.E_PARSE_MUL:
				return "[parse error] expected number with the '*' symbol";
			case ComplexException.E_PARSE_VAL:
				return "[parse error] incorrect number format";
		}
		return "unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (" + this.msg + ").";

		return tmp;
	}
}

