
import Complex.Complex;
import Complex.ComplexException;

public class testCplx
{
	static public void main(String[] args)
	{
		try
		{
		Complex p = new Complex();
		p.setReal(25);
		p.setImag(6);

		System.out.println(p);

		System.out.println(Complex.parseComplex("2+6i"));
		System.out.println(Complex.parseComplex("3-i7"));
		System.out.println(Complex.parseComplex("2-6.3i"));
		System.out.println(Complex.parseComplex("-4+8.1*i"));
		System.out.println(Complex.parseComplex("-2.258+i*9.9"));
		System.out.println(Complex.parseComplex("1+2+6i-i"));
		System.out.println(Complex.parseComplex("1+2i+6i-2"));
		System.out.println(Complex.parseComplex("-i"));
		System.out.println(Complex.parseComplex("+i"));

		System.out.print("\n\n");

		System.out.println(new Complex("2+6i"));
		System.out.println(new Complex("1+2i+6i-2"));
		System.out.println(new Complex("-i"));
		System.out.println(new Complex("2-6.3i"));

		System.out.print("\n\n");
		System.out.println(Complex.parseComplex("2,2+i*2"));
		}
		catch(ComplexException e)
		{
			e.printStackTrace();
		}

	}
}
