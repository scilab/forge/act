/*	Complex.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Allows to handle complex number easily
 */

package Complex;

/** Allows to handle complex number easily
 */
public class Complex
{
	/** Real part */
	private double real;
	/** Imaginary part */
	private double imag;

	/** Initializes a newly created Complex object.
	 *
	 * @param real real part.
	 * @param imag imaginary part.
	 */
	public Complex(double real, double imag)
	{
		this.real = real;
		this.imag = imag;
	}

	/** Initializes a newly created Complex object with a null (0+0i) object.
	 */
	public Complex()
	{
		this(0.0,0.0);
	}

	/** Initializes a newly created Complex object with a string representation
	 * of the complex number.
	 *
	 * @param scplx string representation of the complex number.
	 *
	 * @throws ComplexException if a parse error occur.
	 */
	public Complex(String scplx) throws ComplexException
	{
		this();
		this.fromString(scplx);
	}

	/** Sets the real part of the complex number.
	 *
	 * @param real real part.
	 */
	public void setReal(double real)
	{
		this.real = real;
	}

	/** Sets the imaginary part of the complex number.
	 *
	 * @param imag imaginary part.
	 */
	public void setImag(double imag)
	{
		this.imag = imag;
	}

	/** Gets the real part of the complex number.
	 *
	 * @return real part.
	 */
	public double getReal()
	{
		return this.real;
	}

	/** Gets the imaginary part of the complex number.
	 *
	 * @return imaginary part.
	 */
	public double getImag()
	{
		return this.imag;
	}

	/** Gets the real part of the complex number.
	 *
	 * @return real part.
	 * @see Complex#getReal() getReal()
	 */
	public double Re(Complex cplx)
	{
		return cplx.getReal();	
	}

	/** Gets the imaginary part of the complex number.
	 *
	 * @return imaginary part.
	 * @see Complex#getImag() getImag()
	 */
	public double Im(Complex cplx)
	{
		return cplx.getImag();
	}

	/** Parses the string argument as complex number. The string must
	 * only contain a complex number and the complex number variable
	 * must be i.
	 *
	 * @param val string containing the complex representation to be parsed.
 	 *
	 * @return the complex number represented by the string argument.
	 *
	 * @throws ComplexException if the string does not contain a parsable
	 *	complex number.
	 */
	public static Complex parseComplex(String val) throws ComplexException
	{
		Complex cplx = new Complex();

		String tmp = "";
		int l = val.length();
		int sign = 1;
		boolean imag = false;
		boolean mul = false;
		char ct = 0;
		double value = 0.0;
		
		for(int i = 0; i < l; i++)
		{
			ct = val.charAt(i);
			if(ct > 32)	// Suppression des caractères spéciaux
			{
				if( (ct == '+') || (ct == '-') )
				{
					if (tmp.length() > 0)		// Si il y a un nombre en attente de traitement
					{
						// Si le nombre est complexe on supprime le i (et le signe * si il est présent)
						if(tmp.charAt(0) == 'i')   // Si nombre complexe avec le i devant
						{
							tmp = tmp.substring(1);
							if(tmp.length() > 0)
							{
								if(tmp.charAt(0) == '*')
								{
									tmp = tmp.substring(1);
									mul = true;
								}
							}
							imag = true;
						}
						else if (tmp.charAt(tmp.length()-1) == 'i') // Si nombre complexe avec le i derrière
						{
							tmp = tmp.substring(0,tmp.length()-1);
							if(tmp.charAt(tmp.length()-1) == '*')
							{
								tmp = tmp.substring(0,tmp.length()-1);
								mul = true;
							}
							imag = true;
						}
						
						if(tmp.length() == 0) // Aucun nombre : value = 1.0
						{
							if(mul == true)
							{
								// Erreur : il manque une valeur avant ou après le symbole '*'.
								throw new ComplexException(ComplexException.E_PARSE_MUL, val);
							}
							value = 1.0;
						}
						else
						{
							try
							{
								value = Double.parseDouble(tmp);
							}
							catch (NumberFormatException e)
							{
								// Erreur : La valeur n'est pas un double.
								throw new ComplexException(ComplexException.E_PARSE_VAL, val,e);
							}
						}

						if(imag)
							cplx.setImag(cplx.getImag()+sign*value);
						else
							cplx.setReal(cplx.getReal()+sign*value);
						tmp = "";
					}
					if(ct == '-') sign = -1;	// Signe du nombre suivant
					else sign = 1;
					imag = false;
					mul = false;
				}
				else
				{
					tmp += ct;
				}
			}
			
		}
		if (tmp.length() > 0)		// Si il y a un nombre en attente de traitement
		{
			// Si le nombre est complexe on supprime le i (et le signe * si il est présent)
			if(tmp.charAt(0) == 'i')   // Si nombre complexe avec le i devant
			{
				tmp = tmp.substring(1);
				if(tmp.length() > 0)
				{
					if(tmp.charAt(0) == '*')
					{
						tmp = tmp.substring(1);
						mul = true;
					}
				}
				imag = true;
			}
			else if (tmp.charAt(tmp.length()-1) == 'i') // Si nombre complexe avec le i derrière
			{
				tmp = tmp.substring(0,tmp.length()-1);
				if(tmp.charAt(tmp.length()-1) == '*')
				{
					tmp = tmp.substring(0,tmp.length()-1);
					mul = true;
				}
				imag = true;
			}
			
			if(tmp.length() == 0) // Aucun nombre : value = 1.0
			{
				if(mul == true)
				{
					// Erreur : il manque une valeur avant ou après le symbole '*'.
					throw new ComplexException(ComplexException.E_PARSE_MUL, val);
				}
				value = 1.0;
			}
			else
			{
				try
				{
					value = Double.parseDouble(tmp);
				}
				catch (NumberFormatException e)
				{
					// Erreur : La valeur n'est pas un double.
					throw new ComplexException(ComplexException.E_PARSE_VAL, val,e);
				}
			}

			if(imag)
				cplx.setImag(cplx.getImag()+sign*value);
			else
				cplx.setReal(cplx.getReal()+sign*value);
			tmp = "";
		}
		return cplx;
	}

	/** Parses the string argument as complex number (in the current object).
	 * The string must only contain a complex number and the complex number
	 * variable must be i.
	 *
	 * @param val string containing the complex representation to be parsed.
	 *
	 * @throws ComplexException if the string does not contain a parsable
	 *	complex number.
	 */
	public void fromString(String val) throws ComplexException
	{
		Complex tmp = new Complex();
		tmp = this.parseComplex(val);
		this.real = tmp.getReal();
		this.imag = tmp.getImag();
	}

	/** Returns a string object representing this complex number.
	 *
	 * @param cplxvar complex variable.
	 *
	 * @return a string representation of the value of this object (in base 10).
	 */
	public String toString(String cplxvar)
	{
		String res = "";

		if(this.real != 0.0)
			res += this.real;

		if(this.imag > 0)
		{
			if (this.real != 0.0) res += "+";
			res += this.imag + cplxvar;
		}
		else if (this.imag < 0)
			res += this.imag + cplxvar;

		return res;
	}

	public String toString()
	{
		return this.toString("i");
	}

	

}


