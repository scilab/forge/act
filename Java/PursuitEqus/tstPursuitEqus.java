import PursuitEqus.PursuitEqus;
import PursuitEqus.PursuitEqusException;
import PursuitEqus.ListLambda;
import Complex.ComplexException;
import Complex.ListComplex;

public class tstPursuitEqus
{
	static public void main(String[] args)
	{
		try
		{
			String teq = "y1 := 2*sin(t);\n";
			teq += "y2 := -9*exp(t)+7;";

			PursuitEqus myequs = new PursuitEqus(teq);
			myequs.doIDTest();

			System.out.println(myequs);
			System.out.println("\n============\n");

			String teq2 = "y2 := 2*sin(t);\n";
			teq2 += "y2 := -9*exp(t)+7;";

			PursuitEqus myequs2 = new PursuitEqus(teq2);
			myequs2.doIDTest();

			System.out.println(myequs2);
		}
		catch(PursuitEqusException e)
		{
			e.printStackTrace();
		}
		try
		{
		System.out.println("\n\nTest de ListLambda\n\n");
		String list1 = "3i,7+2i, -8+8i";
		String list2 = "99.2,-8,-i+1,42";
		String list3 = "i*2, 7i+7";
		String list = list1+";"+list2+";"+list3;

		ListLambda lists = new ListLambda(list);

		System.out.println(lists);
		System.out.println("N = " + lists.getNlists());
		System.out.println("list2 = " + lists.getList(1)+"\n");

		lists.add(new ListComplex("42"));
		lists.add("-2i+44,99");

		System.out.println(lists);
		}
		catch(ComplexException e)
		{
			e.printStackTrace();
		}
	}
}
