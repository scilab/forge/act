/*	PursuitEqusException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Thrown to indicate a error when using a PursuitEqus object.
 */

package PursuitEqus;

/** Thrown to indicate a error when using a PursuitEqus object.
 */
public class PursuitEqusException extends Exception
{
	/** unknow error */
	public final static int E_UNKNOW = 0;

	/** [PEBase] output variable must be "y". */
	public final static int E_OUTVAR = 1;
	/** [PEBase] parse id error. */
	public final static int E_IDPARSE = 2;

	/** [PursuitEqus] two equations has the same id. */
	public final static int E_TWOID = 10;

	private int code;
	private String msg;

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 */
	public PursuitEqusException(int code, String msg)
	{
		super();
		this.msg = msg;
		this.code = code;
	}

	/** Constructs a new exception without detail message.
	 */
	public PursuitEqusException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public PursuitEqusException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.msg = msg;
		this.code = code;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public PursuitEqusException(Throwable cause)
	{
		this(0,"",cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case PursuitEqusException.E_OUTVAR:
				return "[PEBase] output variable must be \"y\"";
			case PursuitEqusException.E_IDPARSE:
				return "[PEBase] parse id error";
			case PursuitEqusException.E_TWOID:
				return "[PursuitEqus] two equations have the same id";
		}
		return "unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (" + this.msg + ").";

		return tmp;
	}
}

