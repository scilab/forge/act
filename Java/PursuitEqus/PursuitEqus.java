/*	PursuitEqus.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Does easy test on pursuit/tracking equations.
 */

package PursuitEqus;

/** Does easy test on pursuit/tracking equations.
 */
public class PursuitEqus
{
	/** Output equations list. */
	private java.util.ArrayList<PEBase> equs;
	/** Number of equations. */
	private int nequs;

	/** Initializes a newly created PursuitEqus object with an empty list.
	 *
	 */
	public PursuitEqus()
	{
		this.nequs = 0;
		this.equs = new java.util.ArrayList<PEBase>();
	}

	/** Initializes a newly created PursuitEqus object with a string defined
	 * system.
	 *
	 * @param equs string containing the equations representation.
	 *
	 * @throws PursuitEqusException if a parse error occur.
	 */
	public PursuitEqus(String equs) throws PursuitEqusException
	{
		this();
		this.fromString(equs);
	}

	/** Initializes a PursuitEqus object with a string defined system.
	 *
	 * @param equs string containing the equations representation.
	 *
	 * @throws PursuitEqusException if a parse error occur.
	 */
	public void fromString(String equs) throws PursuitEqusException
	{
		int l = equs.length();
		String tmp = "";
		char ct;
		for(int i = 0; i < l; i++)
		{
			ct = equs.charAt(i);
			if(ct > 32)
			{
				if(ct != ';')
				{
					tmp += ct;
				}
				else
				{
					this.equs.add(new PEBase(tmp));
					tmp = "";
				}
			}
		}
		if (tmp.length() > 0)
		{
			this.equs.add(new PEBase(tmp));
		}
	}

	/** Returns the number of equations.
	 *
	 */
	public int getNequs()
	{
		return this.nequs;
	}

	/** Tests that all the equations have different ID.
	 *
	 * @throws PursuitEqusException if two equations or more have the
	 *	same ID.
	 */
	public void doIDTest() throws PursuitEqusException
	{
		int l = this.equs.size();
		for(int i = 0; i < (l-1) ; i++)
		{
			for (int j = i+1; j < l; j++)
			{
				if(this.equs.get(i).getId() == this.equs.get(j).getId())
				{
					throw new PursuitEqusException(PursuitEqusException.E_TWOID,((Integer)this.equs.get(i).getId()).toString());
				}
			}
		}
	}

	/** Returns a string object representation containing the equations.
	 *
	 * @param sep equations separator.
	 *
	 * @return string representation of the equations.
	 */
	public String toString(String sep)
	{
		String ret = "";
		for(PEBase e: this.equs)
		{
			ret += e.toString() + ";" + sep;
		}
		return ret;
	}

	/** Returns a string object representation containing the equations.
	 *
	 * @return string representation of the equations.
	 *
	 * @see PursuitEqus#PursuitEqus toString(String sep)
	 */
	public String toString()
	{
		return this.toString("\n");
	}

}


