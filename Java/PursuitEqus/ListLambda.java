/*	ListLambda.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Handles a list of lambda lists.
 */

package PursuitEqus;

import Complex.ListComplex;
import Complex.ComplexException;

/** Handles a list of lambda lists.
 */
public class ListLambda
{
	/** List of complex lists. */
	private java.util.ArrayList<ListComplex> lists;

	/** Initializes a newly created ListLambda object with an empty list.
	 *
	 */
	public ListLambda()
	{
		this.lists = new java.util.ArrayList<ListComplex>();
	}

	/** Initializes a newly created ListLambda object with an user defined
	 * String (parsed to extract the list of lambda lists).
	 *
	 * @param lists string containing the lambda lists representation.
	 */
	public ListLambda(String lists) throws ComplexException
	{
		this();
		this.fromString(lists);
	}

	/** Gets the number of lambda lists.
	 *
	 * @return number of lambda lists.
	 */
	public int getNlists()
	{
		return this.lists.size();
	}

	/** Gets the ith complex number list.
	 *
	 * @param ith.
	 *
	 * @return complex number list.
	 */
	public ListComplex getList(int i)
	{
		return this.lists.get(i);
	}

	/** Adds a complex number list.
	 *
	 * @param list complex number list.
	 */
	public void add(ListComplex list)
	{
		this.lists.add(list);
	}

	/** Adds a complex number list (from a string representation).
	 *
	 * @param list string representation of the complex number list.
	 */
	public void add(String list) throws ComplexException
	{
		this.lists.add(new ListComplex(list));
	}

	/** Parses a string containing a list of complex number lists
	 * representation with an user defined separator character.
	 *
	 * @param slists list of complex number lists.
	 * @param sep seperator character.
	 *
	 */
	public void fromString(String slists, char sep) throws ComplexException
	{
		String tmp = "";
		int l = slists.length();
		for(int i = 0; i < l; i++)
		{
			if(slists.charAt(i) > 32)
			{
				if(slists.charAt(i) != sep)
				{
					tmp += slists.charAt(i);
				}
				else
				{
					this.lists.add(new ListComplex(tmp));
					tmp = "";
				}
			}
		}
		if(tmp.length() > 0)
		{
			this.lists.add(new ListComplex(tmp));
		}
	}

	/** Parses a string containing a list of complex number lists
	 * representation.
	 *
	 * @param slists list of complex number lists.
	 *
	 */
	public void fromString(String slists) throws ComplexException
	{
		this.fromString(slists,';');
	}
	
	/** Returns a string object containing a String representation of this
	 * list of complex number lists.
	 *
	 * @param sep character separator used to seperate two complex number
	 *	lists.
	 *
	 * @return object string representation.
	 */
	public String toString(String sep)
	{
		String ret = "";
		for (ListComplex e: this.lists)
		{
			ret += e.toString() + sep;
		}
		return ret;
	}

	/** Returns a string object containing a String representation of this
	 * list of complex number lists.
	 *
	 * @return object string representation.
	 */
	public String toString()
	{
		return this.toString(";\n");
	}
}












