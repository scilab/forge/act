import SysEquLin.StateList;
import SysEquLin.StateListException;

public class tstStateList
{
	static public void main(String[] args)
	{
		try
		{
		StateList p = new StateList();
		System.out.println("x1 = " + p.getState("x1"));
		System.out.println("toto = " + p.getState("toto"));
		System.out.println("x2 = " + p.getState("x2"));
		System.out.println("toto = " + p.getState("toto"));
		System.out.println("x3 = " + p.getState("x3"));
		System.out.println("x4 = " + p.getState("x4"));
		System.out.println("x1 = " + p.getState("x1"));

		System.out.print("\n");

		StateList e = new StateList("  x1 ,   x2   ,  x3 ",',');
		System.out.println(e);
		}
		catch(StateListException e)
		{
			e.printStackTrace();
		}
	}
}
