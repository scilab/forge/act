/*	EquLin.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Describes a linear differential equation of a system.
 *
 *	Must be written like :
 *		<state_i> := a1*<state_1> + ... + an*<state_n>
 *	Example :
 *		x2 := 2*x1 - x2 + 0.5 * x3
 */

package SysEquLin;

/** Describes a linear differential equation of a system.
 *
 *	Must be written like :
 *		<state_i> := a1*<state_1> + ... + an*<state_n>
 *	Example :
 *		x2 := 2*x1 - x2 + 0.5 * x3
 */
public class EquLin
{
	/** differentiated state. */
	private int state;
	/** states list. */
	private java.util.ArrayList<EquLinNome> list;
	/** inputs list. */
	private java.util.ArrayList<EquLinNome> ilist;
	/** Allows to know the equation is simplified. */
	private boolean simplified = false;

	/** Initialize a newly created EquLin object with the user defined
	 * differentiated state (by id).
	 *
	 * @param state differentiated state.
	 */
	public EquLin(int state)
	{
		this.state = state;
		this.list = new java.util.ArrayList<EquLinNome>();
		this.ilist = new java.util.ArrayList<EquLinNome>();
		this.simplified = false;
	}

	/** Initialize a newly created EquLin object with the user defined
	 * differentiated state (by name).
	 *
	 * @param slist states list.
	 * @param name state name.
	 */
	public EquLin(StateList slist, String name)
	{
		this(slist.getState(name));
	}

	/** Creates an empty EquLin object.
	 */
	EquLin()
	{
		this(-1);
	}

	/** Gets the differentiated state.
	 *
	 * @return differentiated state. 
	 */
	public int getState()
	{
		return this.state;
	}

	/** Parses a string object containing a linear differential equation
	 * representation (with a defined syntax).
	 *
	 *	Warning : this function do not use input list. Must be used
	 * only if you are sure that the linear differential equation has no
	 * input.
	 *
	 * @param sequ string object to be parsed.
	 * @param slist states list.
	 *
	 * @throws SysEquLinException if a parse error occur.
	 */
	public void setEquation(String sequ, StateList slist) throws SysEquLinException
	{
		this.setEquation(sequ,slist, new StateList());
	}

	/** Parses a string object containing a linear differential equation
	 * representation (with a defined syntax).
	 *
	 * @param sequ string object to be parsed.
	 * @param slist states list.
	 * @param ulist inputs list.
	 *
	 * @throws SysEquLinException if a parse error occur.
	 */
	public void setEquation(String sequ, StateList slist, StateList ulist) throws SysEquLinException
	{
		this.simplified = false;
		EquLinNome tmpeln;
		String nome;
		String nseq = "";
		int l = sequ.length();
		int signe = 0;
		// Suppression de TOUS les caractères spéciaux.
		for (int i = 0; i < l; i++)
		{
			if(sequ.charAt(i) > 32)
			{
				nseq += sequ.charAt(i);
			}
		}

		// On vérifie que le premier caractère est + ou -
		if( (nseq.charAt(0) != '+') && (nseq.charAt(0) != '-'))
			nseq = "+" + nseq;

		// On analyse la chaine
		l = nseq.length();
		for(int i = 0; i<l;)
		{
			nome = "";
			nome += nseq.charAt(i);
			i++;
			// On prend un nome (séparer par un + ou un -)
			while((nseq.charAt(i) != '+') && (nseq.charAt(i) != '-') && (i < l))
			{
				nome += nseq.charAt(i);
				i++;
				if(i >= l) break;
			}
			tmpeln = new EquLinNome(nome,slist,ulist); // On analyse le nome.
			if(tmpeln.getType() > 0)
				this.list.add(tmpeln);
			else
				this.ilist.add(tmpeln);	
		}
	}

	/** Returns a string object representation of linear differential
	 * equation. States and inputs are given by their number.
	 *
	 * @return string object representation of linear differential equation.
	 */
	public String toString()
	{
		String ret = "<" + this.state + "> :=";
		for (EquLinNome e : this.list)
		{
			ret += " " + e.toString();
		}
		for(EquLinNome e : this.ilist)
		{
			ret += " " + e.toString();
		}
		return ret;
	}

	/** Returns a string object representation of linear output
	 * equation. States and inputs are given by their number.
	 *
	 * @return string object representation of linear output equation.
	 */
	public String toStringY()
	{
		String ret = "y" + this.state + " :=";
		for (EquLinNome e : this.list)
		{
			ret += " " + e.toString();
		}
		for(EquLinNome e : this.ilist)
		{
			ret += " " + e.toString();
		}
		return ret;
	}

	/** Returns a string object representation of linear differential
	 * equation without inputs. States are given by name.
	 *
	 *	Warning : You must used this function only when your linear
	 * differential equation has no input !
	 *
	 * @param slist states list.
	 *
	 * @return string object representation of linear differential equation.
	 */
	public String toString(StateList slist)
	{
		return this.toString(slist,null);
	}

	/** Returns a string object representation of linear ouput
	 * equation without inputs. States are given by name.
	 *
	 *	Warning : You must used this function only when your linear
	 * output equation has no input !
	 *
	 * @param slist states list.
	 *
	 * @return string object representation of linear output equation.
	 */
	public String toStringY(StateList slist)
	{
		return this.toString(slist, null);
	}

	/** Returns a string object representation of linear differential
	 * equation. States and inputs are given by name.
	 *
	 * @param slist states list.
	 * @param ulist inputs list.
	 *
	 * @return string object representation of linear differential equation.
	 */
	public String toString(StateList slist, StateList ulist)
	{
		String ret = "d" + slist.getName(this.state) + " :=";
		for (EquLinNome e : this.list)
		{
			ret += " " + e.toString(slist,ulist);
		}
		if (ulist != null)
		{
			for (EquLinNome e : this.ilist)
			{
				ret += " " + e.toString(slist,ulist);
			}
		}
		return ret;
	}

	/** Returns a string object representation of linear output
	 * equation. States and inputs are given by name.
	 *
	 * @param slist states list.
	 * @param ulist inputs list.
	 *
	 * @return string object representation of linear ouput equation.
	 */
	public String toStringY(StateList slist, StateList ulist)
	{
		String ret = "y" + this.state + " :=";
		for (EquLinNome e : this.list)
		{
			ret += " " + e.toString(slist,ulist);
		}
		if (ulist != null)
		{
			for (EquLinNome e : this.ilist)
			{
				ret += " " + e.toString(slist,ulist);
			}
		}
		return ret;
	}

	/** Simplifies the linear differential equation. It groups coefficient of
	 * a same state together (and only states).
	 *
	 * @return simplified list of basis state (EquLinNome).
	 */
	public java.util.ArrayList<EquLinNome> simplifylist()
	{
		if (this.simplified == false)
		{
			java.util.ArrayList<EquLinNome> nlist = new java.util.ArrayList<EquLinNome>();
			EquLinNome tmp;
			int max = 0;
			for (EquLinNome e : this.list)
			{
				if(e.getState() > max) max = e.getState();
			}
			for(int i = 0; i <= max; i++)
			{
				nlist.add(new EquLinNome(i,0.0,+1));
			}
			for (EquLinNome e : this.list)
			{
				tmp = nlist.get(e.getState());
				tmp.setA(tmp.getA() + e.getA());
			}
			return nlist;
		}
		else
			return this.list;
	}

	/** Simplifies the linear differential equation. It groups coefficient of
	 * a same input together (and only inputs).
	 *
	 * @return simplified list of basis input (EquLinNome).
	 */
	public java.util.ArrayList<EquLinNome> simplifyinput()
	{
		if(this.simplified == false)
		{
			java.util.ArrayList<EquLinNome> nlist = new java.util.ArrayList<EquLinNome>();
			EquLinNome tmp;
			int max = 0;
			for (EquLinNome e : this.ilist)
			{
				if(e.getState() > max) max = e.getState();
			}
			for(int i = 1; i <= max; i++)
			{
				nlist.add(new EquLinNome(i,0.0,-1));
			}
			for (EquLinNome e : this.ilist)
			{
				tmp = nlist.get(e.getState()-1);
				tmp.setA(tmp.getA() + e.getA());
			}
			return nlist;
		}
		else
			return this.ilist;
	}

	/* Simplifies all the linear differential equation (states and inputs).
	 *
	 */
	public void simplify()
	{
		java.util.ArrayList<EquLinNome> tlist = this.simplifylist();
		java.util.ArrayList<EquLinNome> tulist = this.simplifyinput();
		this.list.clear();
		this.ilist.clear();
		this.list = tlist;
		this.ilist = tulist;
		this.simplified = true;
	}

	/** Gets the number of element in the EquLinNome list associated to
	 * states.
	 */
	public int size()
	{
		return this.list.size();
	}

	/** Gets the number of element in the EquLinNome list associated to
	 * inputs.
	 */
	public int sizeinput()
	{
		return this.ilist.size();
	}

	/* Return a new EquLinNome list associated to states (that can be
	 * handle without risks).
	 *
	 * @return copy of the basis states list (EquLinNome).
	 */
	public java.util.ArrayList<EquLinNome> copy()
	{
		java.util.ArrayList<EquLinNome> clist = new java.util.ArrayList<EquLinNome>();
		for (EquLinNome e : this.list)
		{
			clist.add(e);
		}
		return clist;
	}

	/* Return a new EquLinNome list associated to inputs (that can be
	 * handle without risks).
	 *
	 * @return copy of the basis inputs list (EquLinNome).
	 */
	public java.util.ArrayList<EquLinNome> copyinput()
	{
		java.util.ArrayList<EquLinNome> clist = new java.util.ArrayList<EquLinNome>();
		for (EquLinNome e : this.ilist)
		{
			clist.add(e);
		}
		return clist;
	}

	/* Gets the row of the A matrix associated to the current differentiated
	 * state where elements are separated by commas.
	 *
	 *	Reminder : A matrix is the "state matrix"
	 *
	 * @param slist states list.
	 *
	 * @return string object containing the row associated to the current
	 * 	differentiated state of the A matrix.
	 */
	public String toStringMatrixFormA(StateList slist)
	{
		java.util.ArrayList<EquLinNome> mlist = this.simplifylist();
		String ret = "";
		int l = slist.size();
		double tmpval = 0.0;
		for(int i = 0; i < l; i++)
		{
			tmpval = 0.0;
			if (i > 0)
				ret += ",";
			for(EquLinNome e : mlist)
				if(e.getState() == (i+1))
					tmpval = e.getA();
			ret += tmpval;
		}
		return ret;
	}

	/* Gets the row of the B matrix associated to the current differentiated
	 * state where elements are separated by commas.
	 *
	 *	Reminder : B matrix is the "input matrix"
	 *
	 * @param ulist input list.
	 *
	 * @return string object containing the row associated to the current
	 * 	differentiated state of the B matrix.
	 */
	public String toStringMatrixFormB(StateList ulist)
	{
		java.util.ArrayList<EquLinNome> mlist = this.simplifyinput();
		String ret = "";
		int l = ulist.size();
		double tmpval = 0.0;
		for(int i = 0; i < l; i++)
		{
			tmpval = 0.0;
			if (i > 0)
				ret += ",";
			for(EquLinNome e : mlist)
				if(e.getState() == (i+1))
					tmpval = e.getA();
			ret += tmpval;
		}
		return ret;
	}

}


