/*	EquSysLin.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Defines a linear differential equations system.
 */

package SysEquLin;

/** Defines a linear differential equations system.
 */
public class SysEquLin
{
	/** list of linear differential equations. */
	private java.util.ArrayList<EquLin> equs;
	/** list of linear output equations. */
	private java.util.ArrayList<EquLin> outs;
	/** number of linear differential equations. */
	private int nequs;
	/** number of linear output equations. */
	private int nouts;

	/** Creates an empty system.
	 */
	public SysEquLin()
	{
		this.equs = new java.util.ArrayList<EquLin>();
		this.outs = new java.util.ArrayList<EquLin>();
		this.nequs = 0;
		this.nouts = 0;
	}

	/** Initialize an newly defined SysEquLin object with a string object
	 * containing a system definition.
	 *
	 * @param sys string object to be parsed.
	 * @param slist states list.
	 * @param ulist inputs list.
	 *
	 * @throws SysEquLinException if a parse error occur.
	 *
	 * @see SysEquLin#setSystem(String, StateList, StateList) setSystem(String sys, StateList slist, StateList ulist)
	 */
	public SysEquLin(String sys, StateList slist, StateList ulist) throws SysEquLinException
	{
		this();
		this.setSystem(sys, slist, ulist);
	}

	/** Parses a string object containing a system definition (with
	 * a defined syntax).
	 *
	 * @param sys string object to be parsed.
	 * @param slist states list.
	 * @param ulist inputs list.
	 *
	 * @throws SysEquLinException if a parse error occur.
	 */
	public void setSystem(String sys, StateList slist, StateList ulist) throws SysEquLinException
	{
		String nsys = "";
		String tmp = "";
		String tequ[];
		EquLin e;
		int l = sys.length();
		// Suppression des espaces (et des caractères spéciaux)
		for (int i = 0; i < l; i++)
		{
			if (sys.charAt(i) > 32)
			{
				nsys += sys.charAt(i);
			}
		}
		l = nsys.length();
		for(int i = 0; i < l; i++)	// Analyse de la chaine
		{
			//i++;	// on ignore le 'd' initial 
			// On récupère chaque équation différentiel (séparé par un ';').
			while( (i < l) && (nsys.charAt(i) != ';'))
			{
				tmp += nsys.charAt(i);
				i++;
			}
			// traitement temporaire
			tequ = tmp.split(":=");
			if(tequ[0].charAt(0) == 'd')
			{
				e = new EquLin(slist,tequ[0].substring(1));
				e.setEquation(tequ[1],slist,ulist);
				this.equs.add(e);
				this.nequs++;
			}
			else if(tequ[0].charAt(0) == 'y')
			{
				String tmp2 = tequ[0].substring(1);
				int id = 0;
				try
				{
					id = Integer.parseInt(tmp2);
				}
				catch(NumberFormatException ex)
				{
					throw new SysEquLinException(SysEquLinException.E_YPARSE_ID,tmp,ex);
				}

				if(id < 1)
				{
					throw new SysEquLinException(SysEquLinException.E_ID,tmp);
				}
				e = new EquLin(id);
				e.setEquation(tequ[1],slist,ulist);
				this.outs.add(e);
				this.nouts++;
			}
			else
				throw new SysEquLinException(SysEquLinException.E_TYPE_EQU,tmp);
			tmp = "";
		}
	}

	/* Puts the linear differential equations in order (<1>, <2>, ... <n>).
	 *
	 */
	public void ordering()
	{
		java.util.ArrayList<EquLin> tequs = new java.util.ArrayList<EquLin>();
		int l = this.equs.size();
		for(int i = 0; i < l; i++)
		{
			for(int j = 0; j < l; j++)
			{
				if(this.equs.get(j).getState() == (i+1))
				{
					tequs.add(this.equs.get(j));
					continue;
				}
			}
		}
		this.equs.clear();
		this.equs = tequs;
	}

	/* Puts the linear output equations in order (<1>, <2>, ... <n>).
	 *
	 */
	public void orderingOuput()
	{
		java.util.ArrayList<EquLin> tequs = new java.util.ArrayList<EquLin>();
		int l = this.outs.size();
		for(int i = 0; i < l; i++)
		{
			for(int j = 0; j < l; j++)
			{
				if(this.outs.get(j).getState() == (i+1))
				{
					tequs.add(this.outs.get(j));
					continue;
				}
			}
		}
		this.outs.clear();
		this.outs = tequs;
	}

	/** Gets the number of linear differential equations.
	 *
	 * @return number of linear differential equations.
	 */
	public int getNequs()
	{
		return this.nequs;
	}

	/** Gets the number of linear differential equations.
	 *
	 * @return number of linear differential equations.
	 */
	public int getNouts()
	{
		return this.nouts;
	}

	/** Returns a string object containing a representation of the linear
	 * differential equations system with user defined separator character
	 * (print numbers).
	 *
	 * @param sep separator character between each equation.
	 *
	 * @return string representation of the system.
	 */
	public String toString(String sep)
	{
		String ret = "";
		for(EquLin e : this.equs)
		{
			ret += e + ";" + sep;
		}
		for(EquLin e : this.outs)
		{
			ret += e.toStringY() + ";" + sep;
		}
		return ret;
	}

	/** Returns a string object containing a representation of the linear
	 * differential equations system with line feed as separator character
	 * (print numbers).
	 *
	 * @return string representation of the system.
	 */
	public String toString()
	{
		return this.toString("\n");
	}

	/** Returns a string object containing a representation of the linear
	 * differential equations system with user defined separator character
	 * (print names).
	 *
	 * @param sep separator character between each equation.
	 *
	 * @return string representation of the system.
	 */
	public String toString(StateList slist, StateList ulist, String sep)
	{
		String ret = "";
		for(EquLin e : this.equs)
		{
			ret += e.toString(slist,ulist) + ";" + sep;
		}
		for(EquLin e : this.outs)
		{
			ret += e.toStringY(slist,ulist) + ";" + sep;
		}
		return ret;
	}

	/** Returns a string object containing a representation of the linear
	 * differential equations system with line feed as separator character
	 * (print names).
	 *
	 * @return string representation of the system.
	 */
	public String toString(StateList slist, StateList ulist)
	{
		return this.toString(slist, ulist, "\n");
	}

	/** Returns a string object containing a representation of the A matrix
	 * (where each rows are separated by semicolons).
	 *
	 * @param slist state list.
	 *
	 * @return string representation of the A matrix.
	 */
	public String toStringMatrixFormA(StateList slist)
	{
		String ret = "";
		if(this.equs.size() > 0)
		{
			this.ordering();
			for(EquLin e : this.equs)
			{
				ret += ";" + e.toStringMatrixFormA(slist);
			}
			ret = ret.substring(1); // Suppression du premier ';'.
		}
		return ret;
	}

	/** Returns a string object containing a representation of the B matrix
	 * (where each rows are separated by semicolons).
	 *
	 * @param ulist input list.
	 *
	 * @return string representation of the B matrix.
	 */
	public String toStringMatrixFormB(StateList ulist)
	{
		String ret = "";
		if(this.equs.size() > 0)
		{
			this.ordering();
			for(EquLin e : this.equs)
			{
				ret += ";" + e.toStringMatrixFormB(ulist); 
			}
			ret = ret.substring(1); // Suppression du premier ';'.
		}
		return ret;
	}

	/** Returns a string object containing a representation of the C matrix
	 * (where each rows are separated by semicolons).
	 *
	 * @param slist state list.
	 *
	 * @return string representation of the C matrix.
	 *
	 * @see SysEquLin#toStringMatrixFormA(StateList) toStringMatrixFormA(StateList slist)
	 */
	public String toStringMatrixFormC(StateList slist)
	{
		String ret = "";
		if(this.outs.size() > 0)
		{
			this.orderingOuput();
			for(EquLin e : this.outs)
			{
				ret += ";" + e.toStringMatrixFormA(slist);
			}
			ret = ret.substring(1); // Suppression du premier ';'.
		}
		return ret;
	}

	/** Returns a string object containing a representation of the D matrix
	 * (where each rows are separated by semicolons).
	 *
	 * @param ulist input list.
	 *
	 * @return string representation of the D matrix.
	 *
	 * @see SysEquLin#toStringMatrixFormB(StateList) toStringMatrixFormB(StateList ulist)
	 */
	public String toStringMatrixFormD(StateList ulist)
	{
		String ret = "";
		if(this.outs.size() > 0)
		{
			this.ordering();
			for(EquLin e : this.outs)
			{
				ret += ";" + e.toStringMatrixFormB(ulist); 
			}
			ret = ret.substring(1);// Suppression du premier ';'.
		}
		return ret; 
	}

}


