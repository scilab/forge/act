/*	StateListException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Gestion des erreurs dans les listes d'état.
 */

package SysEquLin;

public class StateListException extends Exception
{
	/** unknow error. */
	public final static int E_UNKNOW = 0;
	/** [state list error] a state must not start with a digit */
	public final static int E_NODIGIT = 1;

	private int code;
	private String msg;

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 */
	public StateListException(int code, String msg)
	{
		super();
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception without detail message.
	 *
	 */
	public StateListException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public StateListException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public StateListException(Throwable cause)
	{
		this(0,"",cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case StateListException.E_NODIGIT:
				return "[state list error] a state must not start with a digit";
		}
		return "Unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error (StateList) " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (" + this.msg + ").";

		return tmp;
	}


}













