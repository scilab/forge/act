/*	StateListBase.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Basis element of a state list. It associates a state name to a number.
 */

package SysEquLin;

/** Basis element of a state list. It associates a state name to a number.
 */
public class StateListBase
{
	private String name;
	private int value;

	/** Initializes a newly created State with user defined state name and
	 * state value.
	 */
	public StateListBase(String name, int value)
	{
		this.name = name;
		this.value = value;
	}

	/** Creates an empty state : without name and without correct id. 
	 */
	public StateListBase()
	{
		this(null,-1);
	}

	/** Sets the state name.
	 *
	 * @param name new name of the state.
	 */
	public void setName(String name)
	{
		this.name = name;
	}	

	/** Sets the state value.
	 *
	 * @param value new value of the state.
	 */
	public void setValue(int value)
	{
		this.value = value;
	}

	/** Gets the state name.
	 *
	 * @return state name.
	 */
	public String getName()
	{
		return this.name;
	}

	/** Gets the state value.
	 *
	 * @return state value.
	 */
	public int getValue()
	{
		return this.value;
	}

	/** Returns a string object representing this state.
	 * 	<name> = <value>
	 *
	 * @return string representation of this state.
	 */
	public String toString()
	{
		return this.name + " = " + this.value;
	}
}



