/*	StateList.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Lists all the states of the system.
 */

package SysEquLin;

/** Lists all the states of the system.
 */
public class StateList
{
	/** Value of the next added state. */
	private int astate = 1;

	/** List of states. */
	private java.util.ArrayList<StateListBase> list;

	/** Creates an empty states list.
	 */
	public StateList()
	{
		this.list = new java.util.ArrayList<StateListBase>();
	}

	/** Initializes a newly created states list with a string object
	 * containing all the state.
	 *
	 * @param slist string object containing the states list separated by
	 *	the sep character.
	 * @param sep separator character. May be, for example : ',' ';'
	 *
	 * @throws StateListException if one or more state have a bad name.
	 * 
	 * @see StateList#fromString(String, char) fromString(String slist, char sep)
	 */
	public StateList(String slist, char sep) throws StateListException
	{
		this();
		this.fromString(slist,sep);
	}

	/** Initializes a newly created states list with a string object
	 * containing all the state.
	 *
	 * @param slist string object containing the states list separated by
	 *	a comma.
	 *
	 * @throws StateListException if one or more state have a bad name.
	 *
	 * @see StateList#StateList(String, char) StateList(String slist, char sep)
	 */
	public StateList(String slist) throws StateListException
	{
		this(slist,',');
	}

	/** Gets the size of the list (number of elements).
	 *
	 * @return number of elements in the list.
	 */
	public int size()
	{
		return this.list.size();
	}

	/** Allows to know if a state is already define in the list or not
	 * (search by name).
	 *
	 * @param name name of the state.
	 *
	 * @return true if the state already exist and else false.
	 */
	public boolean exist(String name)
	{
		for(StateListBase e : this.list)
		{
			if(e.getName().equals(name))
				return true;
		}
		return false;
	}

	/** Allows to know if a state is already define in the list or not
	 * (search by value).
	 *
	 * @param state value of the state.
	 *
	 * @return true if the state already exist and else false.
	 */
	public boolean exist(int state)
	{
		for(StateListBase e : this.list)
		{
			if(e.getValue() == state)
				return true;
		}
		return false;
	}

	/** Adds a state into the list (define the value automatically).
	 *
	 * @param name state name.
	 *
	 * @return false if the state already exist (the state is not added)
	 * 	and else true.
	 *
	 * @throws StateListException if one or more state have a bad name.
	 */
	public boolean add(String name) throws StateListException
	{
		if(this.exist(name) == false)
		{
			if( (name.charAt(0) >= '0') && (name.charAt(0) <= '9')) // Erreur : Les états ne doivent pas commencer pas un chiffre
				throw new StateListException(StateListException.E_NODIGIT, name);
			this.list.add(new StateListBase(name,this.astate));
			this.astate++;
			return true;
		}
		else
			return false;
	}

	/** Gets a state by name. The 'autoadd' parameter allows to know if
	 * the state is added when it does not exist.
	 *
	 * @param name state name.
	 * @param autoadd if autoadd is true the state is automatically added
	 *	when it does not exist.
	 *
	 * @return state number (or -1 if the state does not exist).
	 *
	 * @throws StateListException if the state have a bad name.
	 */
	public int getState(String name, boolean autoadd) throws StateListException
	{
		this.getState(name);
		if (autoadd)
		{
			if (this.add(name) == true)
			{
				return (this.astate - 1);
			}
		}
		return -1;
	}

	/** Gets a state by name.
	 *
	 * @param name state name.
	 *
	 * @return state number (or -1 if the state does not exist).
	 */
	public int getState(String name)
	{
		for(StateListBase e: this.list)	
		{
			if(e.getName().equals(name))
				return e.getValue();
		}
		return -1;
	}

	/** Gets a state by name. The state is automatically added when it's
	 * does not exist.
	 *
	 * @param name state name.
	 *
	 * @return state number (or -1 if the state does not exist).
	 *
	 * @throws StateListException if the state have a bad name.
	 */
	public int getStateA(String name) throws StateListException
	{
		return this.getState(name,true);
	}

	/** Gets the state name by it's id. Returns null if no state has this
	 * id.
	 *
	 * @param state state number.
	 *
	 * @return state name.
	 */
	public String getName(int state)
	{
		if (state != 0)
		{
			for(StateListBase e: this.list)
			{
				if(e.getValue() == state)
					return e.getName();
			}
			return null;
		}
		else
		{
			return "<cst>";
		}
	}

	/** Gets the state list from a string object.
	 *
	 * @param slist string object containing the state list.
	 * @param sep separator character. For example : ',' ';' ':'.
	 * 	(Do not use special character !).
	 *
	 * @throws StateListException if one or more state have a bad name.
	 */
	public void fromString(String slist, char sep) throws StateListException
	{
		String tmp = "";
		int l = slist.length();
		for (int i = 0; i < l; i++)
		{
			if(slist.charAt(i) > 32)
			{
				if(slist.charAt(i) != sep)
					tmp += slist.charAt(i);
				else
				{
					this.add(tmp);
					tmp = "";
				}
			}
		}
		this.add(tmp);
	}

	/** Returns a string object representing this state list with an
	 * user defined separator character.
	 *
	 * @param sep separator character.
	 *
	 * @return string representation of this state list.
	 */
	public String toString(String sep)
	{
		String ret = "";
		for(StateListBase e : this.list)
		{
			ret += e.getName() + sep;
		}
		ret = ret.substring(0,ret.length()-1);
		return ret;
	}

	/** Returns a string object representing this state list with a comma
	 * as separator character.
	 *
	 * @return string representation of this state list.
	 */
	public String toString()
	{
		return this.toString(",");
	}

	/** Returns a string object representing this state list with a line feed
	 * as separator character.
	 *
	 * @return string representation of this state list.
	 */
	public String toStringList()
	{
		return this.toString("\n");
	}
}


