/*	Toolboxmainwindow.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fenêtre principale contenant toutes les autres.
 */

package Windows;

import Windows.IntroPanel2;
import Windows.Sys;
import Windows.Stab;
import Windows.Pursuit;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.io.File;

import java.lang.System;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;

import java.util.Locale;
import PropertyFile.i18n;
import PropertyFile.PropertyFileException;


public class Toolboxmainwindow extends JFrame
{
	private JMenuBar menuBar;
	private JMenu mainmenu;
	private JMenu confmenu;
	private JMenu helpmenu;

	// Sous-menu de mainmenu ("Fichier")
	private JMenuItem mopenfile;
	private JMenuItem msavefile ;
	private JMenuItem mquit;

	// Sous-menu de confmenu ("Configuration")
	//private JMenuItem mconfig;
	private JMenu mlang;

	// Sous-sous menu de confmenu : confmenu/mlang ("Langue")
	private JRadioButtonMenuItem mfr;
	private JRadioButtonMenuItem men;
	private JRadioButtonMenuItem mde;
	private ButtonGroup langgroup = new ButtonGroup();

	// Sous-menu de helpmenu ("Aide")
	private JMenuItem mhelp;
	private JMenuItem mapropos;

	// Onglet
	private JTabbedPane tab;

	// Différent onglets :
	private IntroPanel2 tab_intro;
	private Sys tab_system;
	private Stab tab_stab;
	private Pursuit tab_pursuit;

	private Locale def;
	private i18n lang;

	private CloudData cd;

	public Toolboxmainwindow(CloudData cloud)
	{	
		
		this.cd = cloud;
		this.def = Locale.getDefault();
		try {
			this.lang = this.cd.getTextLang();
			//this.lang = new i18n(this.cd.geti18nDir(), new Locale(def.getLanguage(),def.getCountry()));
			//cd.setLang(new i18n(this.cd.geti18nDir(), new Locale(def.getLanguage(),def.getCountry()),"error"));
		}
		catch(PropertyFileException e) {
			JOptionPane jop = new JOptionPane();
			jop.showMessageDialog(null, e.getMessage() + "[" + cd.geti18nDir() + "]", "Erreur", JOptionPane.ERROR_MESSAGE);
		}
		//this.cd.setLang(new i18n("../", new Locale(def.getLanguage(),def.getCountry()), "error"));
		int i = 0;
		String glang = this.cd.getLanguage();
		//System.out.println("..."+glang+"...");
		if (glang.equals("fr")) {
			//System.out.println("plop fr");
			i = 2;
		}
		else if (glang.equals("en")) {
			//System.out.println("plop en");
			i = 0;
		}
		else if (glang.equals("de")) {
			//System.out.println("plop de");
			i = 1;
		}
		//System.out.println("..."+glang+"...");
		

		//Définit un titre pour votre fenêtre
		this.setTitle("Toolbox");
		//Définit une taille pour celle-ci ; ici, 400 px de large et 500 px de haut
		this.setSize(600, 400);
		//Nous allons maintenant dire à notre objet de se positionner au centre
		this.setLocationRelativeTo(null);
		//Ferme-toi lorsqu'on clique sur "Fermer" !
        	this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.addWindowListener(new WindowListener() {
			public void windowClosed(WindowEvent arg0) {}
          		public void windowActivated(WindowEvent arg0) {}
			public void windowClosing(WindowEvent arg0) {

                		JOptionPane jop = new JOptionPane();			
				int option = jop.showConfirmDialog(null, lang.getString("save?"), lang.getString("closing"), 												JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			
				if(option != JOptionPane.NO_OPTION && option != JOptionPane.CANCEL_OPTION && option != JOptionPane.CLOSED_OPTION) {
					save();
				}
				System.exit(0);
            		}
			public void windowDeactivated(WindowEvent arg0) {}
            		public void windowDeiconified(WindowEvent arg0) {}
            		public void windowIconified(WindowEvent arg0) {}
            		public void windowOpened(WindowEvent arg0) {}
		});

		this.initComponent(i);


		this.setVisible(true);
	}

	/**
	*	Initialise la classe Toolboxmainwindow
	*/
	public void initComponent(int e) {

		//Définit un titre pour votre fenêtre
		this.setTitle("Toolbox");

		this.menuBar = new JMenuBar();
		this.mainmenu = new JMenu(this.lang.getString("file"));
		this.confmenu = new JMenu(this.lang.getString("configuration"));
		this.helpmenu = new JMenu(this.lang.getString("help")); 

		// Sous-menu de mainmenu ("Fichier")
		this.mopenfile = new JMenuItem(this.lang.getString("open"));
		this.msavefile = new JMenuItem(this.lang.getString("save"));
		this.mquit = new JMenuItem(this.lang.getString("exit"));

		// Sous-menu de confmenu ("Configuration")
		//this.mconfig = new JMenuItem(this.lang.getString("configuration"));
		this.mlang = new JMenu(this.lang.getString("language"));

		// Sous-sous menu de confmenu : confmenu/mlang ("Langue")
		this.mfr = new JRadioButtonMenuItem(this.lang.getString("french"));
		this.men = new JRadioButtonMenuItem(this.lang.getString("english"));
		this.mde = new JRadioButtonMenuItem(this.lang.getString("german"));

		// Sous-menu de helpmenu ("Aide")
		this.mhelp = new JMenuItem(this.lang.getString("help"));
		this.mapropos = new JMenuItem(this.lang.getString("about"));

		// Onglet
		this.tab = new JTabbedPane(JTabbedPane.TOP);

		// Différent onglets :
		this.tab_intro = new IntroPanel2(this.lang, this.cd);
		this.tab_system = new Sys(this.lang, this.cd);
		this.tab_pursuit = new Pursuit(this.lang, this.cd);
		this.tab_stab = new Stab(this.lang, this.cd, this.tab_pursuit);

		this.initMenu(e);
		this.initTab();
	}

	/**
	*	Initialise le menu de la classe Toolboxmainwindow
	*/
	private void initMenu(int e)
	{
		// Menu "Fichier"
		this.mainmenu.add(mopenfile);
		this.mainmenu.add(msavefile);
		this.mainmenu.addSeparator();
		this.mainmenu.add(mquit);

		this.mopenfile.addActionListener(new OpenFileListener());
		this.msavefile.addActionListener(new SaveFileListener());
		this.mquit.addActionListener(new QuitListener());

		this.langgroup.add(mfr);
		this.langgroup.add(men);
		this.langgroup.add(mde);
		switch(e)
		{
			case 0 : this.men.setSelected(true);break;
			case 1 : this.mde.setSelected(true);break;
			case 2 : this.mfr.setSelected(true);break;
		}
		this.mlang.add(mfr);
		this.mlang.add(men);
		this.mlang.add(mde);

		this.mfr.addActionListener(new LangListener());
		this.men.addActionListener(new LangListener());
		this.mde.addActionListener(new LangListener());

		//this.confmenu.add(mconfig);
		this.confmenu.add(mlang);

		//this.mconfig.addActionListener(new ConfigListener());

		this.helpmenu.add(mhelp);
		this.helpmenu.add(mapropos);

		this.mhelp.addActionListener(new HelpListener());
		this.mapropos.addActionListener(new AboutListener());

		this.menuBar.add(mainmenu);
		this.menuBar.add(confmenu);
		this.menuBar.add(helpmenu);
		this.setJMenuBar(menuBar);
		menuBar.updateUI();
	}

	/**
	*	Initialiseles onglets de la classe Toolboxmainwindow
	*/
	private void initTab()
	{
		this.tab.add(this.lang.getString("introduction"),tab_intro);
		this.tab.add(this.lang.getString("system"),tab_system);
		this.tab.add(this.lang.getString("stabilization"),tab_stab);
		this.tab.add(this.lang.getString("tracking"),tab_pursuit);

		//On prévient notre JFrame que ce sera notre JPanel qui sera son contentPane
		this.setContentPane(tab);
	}

	/**
	*	Réinitialise la classe Toolboxmainwindow lors d'un changement de fichier de lague
	*/
	public void reinitComponent() {

		this.mainmenu.setText(this.lang.getString("file"));
		this.confmenu.setText(this.lang.getString("configuration"));
		this.helpmenu.setText(this.lang.getString("help")); 

		// Sous-menu de mainmenu ("Fichier")
		this.mopenfile.setText(this.lang.getString("open"));
		this.msavefile.setText(this.lang.getString("save"));
		this.mquit.setText(this.lang.getString("exit"));

		// Sous-menu de confmenu ("Configuration")
		//this.mconfig.setText(this.lang.getString("configuration"));
		this.mlang.setText(this.lang.getString("language"));

		// Sous-sous menu de confmenu : confmenu/mlang ("Langue")
		this.mfr.setText(this.lang.getString("french"));
		this.men.setText(this.lang.getString("english"));
		this.mde.setText(this.lang.getString("german"));

		// Sous-menu de helpmenu ("Aide")
		this.mhelp.setText(this.lang.getString("help"));
		this.mapropos.setText(this.lang.getString("about"));

		this.tab.setTitleAt(0, this.lang.getString("introduction"));
		this.tab.setTitleAt(1, this.lang.getString("system"));
		this.tab.setTitleAt(2, this.lang.getString("stabilization"));
		this.tab.setTitleAt(3, this.lang.getString("tracking"));

		// Différent onglets :
		this.tab_intro.reinitComponent(this.lang);
		this.tab_system.reinitComponent(this.lang);
		this.tab_stab.reinitComponent(this.lang);
		this.tab_pursuit.reinitComponent(this.lang);
	}


	// Demande d'ouverture d'un fichier
	public class OpenFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			open();
		}
	}

	// Demande d'enregistrement d'un fichier
	public class SaveFileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			save();
		}
	}

	// Quitter
	public class QuitListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			JOptionPane jop = new JOptionPane();			
			int option = jop.showConfirmDialog(null, lang.getString("save?"), lang.getString("closing"),
											JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
			
			if(option != JOptionPane.NO_OPTION && option != JOptionPane.CANCEL_OPTION && option != JOptionPane.CLOSED_OPTION)
			{
				save();
			}

			System.out.println("Quitter");
			System.exit(0);
		}
	}

	// Configuration
/*	public class ConfigListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Configuration config = new Configuration(lang, cd);
		}
	}
*/

	// Langue
	public class LangListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			JRadioButtonMenuItem source = (JRadioButtonMenuItem)e.getSource();
			int i;

			if(source == mfr) {
				try {
					cd.setTextLang("fr","");
					lang = cd.getTextLang();
					cd.setLang(new i18n(cd.geti18nDir(), new Locale("fr","error")));
				}
				catch(PropertyFileException e1) {
					JOptionPane jop1 = new JOptionPane();
					jop1.showMessageDialog(null, e1.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
				}
				//cd.setLang(new i18n("../CloudData/",new Locale("fr","FR"), "error"));
				i=2;
			}
			else if(source == men) {
				try {
					cd.setTextLang("en","");
					lang = cd.getTextLang();
					cd.setLang(new i18n(cd.geti18nDir(), new Locale("en","error")));
				}
				catch(PropertyFileException e2) {
					JOptionPane jop2 = new JOptionPane();
					jop2.showMessageDialog(null, e2.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
				}
				//cd.setLang(new i18n(this.cd.geti18nDir(),new Locale("en","GB"), "error"));
				i=0;
			}
			else if(source == mde) {
				try {
					cd.setTextLang("de","");
					lang = cd.getTextLang();
					cd.setLang(new i18n(cd.geti18nDir(), new Locale("de","error")));
				}
				catch(PropertyFileException e3) {
					JOptionPane jop3 = new JOptionPane();
					jop3.showMessageDialog(null, e3.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
				}
				//cd.setLang(new i18n(cd.geti18nDir(),new Locale("en","US"), "error"));
				i=1;
			}
			else {
				i=0;
			}

			reinitComponent();
			//updateUI();
			
		}
	}

	// Aide
	public class HelpListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Trucs à faire quand on appel l'aide
			System.out.println("Aide");
		}
	}

	// A propos
	public class AboutListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			// Trucs à faire quand on veut en savoir plus sur l'appli.
			System.out.println("A propos");
		}
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		try{    
			   JFileChooser chooser = new JFileChooser();
				    
			   // Dossier Courant
			  chooser.setCurrentDirectory(new  File("."+File.separator)); 
					    
			   //Affichage et récupération de la réponse de l'utilisateur
			   int reponse = chooser.showDialog(chooser,this.lang.getString("Open"));
				     
			  // Si l'utilisateur clique sur OK
			  if  (reponse == JFileChooser.APPROVE_OPTION){
				
				 // Récupération du chemin du fichier
				String  fichier= chooser.getSelectedFile().toString(); 
				
				 //Ecriture du fichier
				cd.load(fichier);
			   }
		}
		catch(HeadlessException he) {
			he.printStackTrace();
			JOptionPane jop4 = new JOptionPane();
			jop4.showMessageDialog(null, he.getMessage(), this.lang.getString("error"), JOptionPane.ERROR_MESSAGE);
		}
		catch(CloudDataException e) {
			e.printStackTrace();
			JOptionPane jop5 = new JOptionPane();
			jop5.showMessageDialog(null, e.getMessage(), this.lang.getString("error"), JOptionPane.ERROR_MESSAGE);
		}

		tab_system.open();
		tab_stab.open();
		tab_pursuit.open();
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void save() {

		tab_system.save();
		tab_stab.save();
		tab_pursuit.save();

		try{    
			   JFileChooser chooser = new JFileChooser();
				    
			   // Dossier Courant
			  chooser.setCurrentDirectory(new  File("."+File.separator)); 
					    
			   //Affichage et récupération de la réponse de l'utilisateur
			   int reponse = chooser.showDialog(chooser,this.lang.getString("Save"));
				     
			  // Si l'utilisateur clique sur OK
			  if  (reponse == JFileChooser.APPROVE_OPTION){
				
				 // Récupération du chemin du fichier
				String  fichier= chooser.getSelectedFile().toString();
				File pfile = new File(fichier);
				if(pfile.exists())
				{
					JOptionPane jop = new JOptionPane();			
					int option = jop.showConfirmDialog(null, lang.getString("erase?"), lang.getString("save2"), 												JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			
					if(option == JOptionPane.YES_OPTION) {
						cd.save(fichier);
					}
					else	this.save();
				}
				
				 //Ecriture du fichier
				//cd.save(fichier);
			   }
		}catch(HeadlessException he){
			he.printStackTrace();
			JOptionPane jop6 = new JOptionPane();
			jop6.showMessageDialog(null, he.getMessage(), this.lang.getString("error"), JOptionPane.ERROR_MESSAGE);
		}
		catch(CloudDataException e)
		{
			e.printStackTrace();
			JOptionPane jop7 = new JOptionPane();
			jop7.showMessageDialog(null, e.getMessage(), this.lang.getString("error"), JOptionPane.ERROR_MESSAGE);
		}
	}
}









