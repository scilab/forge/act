/*	Assistant.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Assistant d'écriture de matrices.
 */

package Windows;

import Windows.AssGrid;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
import static java.lang.Math.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;

import java.util.ArrayList;

import PropertyFile.i18n;

public class Assistant extends JFrame {

	//private ArrayList<JTextField> Mat;
	//private JLabel text;
	private AssGrid assgrid;

	private i18n lang;

	private CloudData cd;

	public Assistant(int a, int b, JTextField mat, i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();
		this.assgrid = new AssGrid(a, b, mat, lang, cd);

		//Définit une taille pour celle-ci ;
		this.setSize(50*(b+2), 50*(a+1));

		this.setLayout(new BorderLayout());

		//JPanel tab = new JPanel();
		//tab.setLayout(new GridBagLayout());
		//this.setLayout(new GridBagLayout());
		//GridBagConstraints c = new GridBagConstraints();


/*			//"Matrice : "
		c.fill = GridBagConstraints.VERTICAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = (a-1)/2;
		//c.gridheight = a;
		this.add(text,c);

		int k;
		for(k=0;k<a*b;k++) {
			Mat.add(k, new JTextField());
			Mat.get(k).setPreferredSize(new Dimension(40, 40));
		}

		int i,j;
		for(i=0;i<a;i++) {

			for(j=0;j<b;j++) {

					//Zones te teste
				c.fill = GridBagConstraints.HORIZONTAL;
				c.weightx = 1.0;
				c.weighty = 1.0;
				c.gridx = j+1;
				c.gridy = i;
				c.gridwidth = 1;
				this.add(Mat.get(i*b+j),c);
			}
		}
*/

		JPanel control = new JPanel();

		JButton okBouton = new JButton("Ok");
		okBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {				
				assgrid.transform();
				setVisible(false);
			}		
		});
		
		JButton cancelBouton = new JButton(lang.getString("Cancel"));
		cancelBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}			
		});
		
		control.add(okBouton);
		control.add(cancelBouton);

		this.getContentPane().add(assgrid, BorderLayout.CENTER);
		this.getContentPane().add(control, BorderLayout.SOUTH);

		this.setVisible(true);
	}


	/**
	*	Initialise la classe Assistant
	*/
	public void initComponent() {

		//this.Mat = new ArrayList<JTextField>();
		//this.text = new JLabel(this.lang.getString("matrix2"));

		//Définit un titre pour votre fenêtre
		this.setTitle(this.lang.getString("ass_mat"));
		//Nous allons maintenant dire à notre objet de se positionner au centre
		//this.setLocationRelativeTo(null);
		//On prévient notre JFrame que ce sera notre JPanel qui sera son contentPane
		//this.setContentPane(tab);


	}
}

