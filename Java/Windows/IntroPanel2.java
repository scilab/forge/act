/*	IntroPanel2.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page d'acceuil permettant de trouver les chemins jusqu'aux logiciels Scilab et Maxima si le logiciel ne le trouve pas tout seul ou après un changement de version. v2
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.io.File;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;

import PropertyFile.i18n;
import PropertyFile.PropertyFileException;

public class IntroPanel2 extends JPanel
{
	private String sci;
	private String max;

	private JPanel listelem;
	private JPanel elem_sci;
	private JPanel elem_max;
	private JPanel selem_sci;
	private JPanel selem_max;

	private JLabel intro_txt;
	private JLabel scilab_txt;
	private JLabel maxima_txt;
	private JTextField scilab_path;
	private JTextField maxima_path;

	private JButton scilab_but;
	private JButton maxima_but;
	private JButton Save;

	private i18n lang;

	private CloudData cd;

	public IntroPanel2(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Text : "Détection des logiciels"
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(intro_txt,c);

		// Text	: "Scilab : "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(scilab_txt,c);

		// Text : Version de Scilab
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(scilab_path,c);

		// Bouton : "Trouver Scilab manuellement"
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 2;
		this.add(scilab_but,c);

		// Text : "Maxima : "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(maxima_txt,c);	

		// Text : Version de Maxima
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(maxima_path,c);

		// Bouton : "Trouver Maxima manuellement"
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 4;
		c.gridwidth = 2;
		this.add(maxima_but,c);


		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 6;
		this.add(Save,c);


		scilab_but.addActionListener(new IntroListener());
		maxima_but.addActionListener(new IntroListener());
		Save.addActionListener(new SaveListener());
	}

	public class IntroListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			JButton source = (JButton) e.getSource();

			try{    
			   	JFileChooser chooser = new JFileChooser();
				    
			  	 // Dossier Courant
			 	 chooser.setCurrentDirectory(new  File("."+File.separator)); 
					    
			 	  //Affichage et récupération de la réponse de l'utilisateur
			 	  int reponse = chooser.showDialog(chooser,lang.getString("Path"));
				     
			 	 // Si l'utilisateur clique sur OK
				  if  (reponse == JFileChooser.APPROVE_OPTION){
				
					 // Récupération du chemin du fichier
					String  fichier = chooser.getSelectedFile().toString(); 
				
					 //Ecriture du fichier
					if (source == scilab_but)	scilab_path.setText(fichier);
					else if (source == maxima_but)	maxima_path.setText(fichier);
			 	  }
			}
			catch(HeadlessException he) {
				he.printStackTrace();
				JOptionPane jop = new JOptionPane();
				jop.showMessageDialog(null, he.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public class SaveListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {
			
			cd.setScilabPath(scilab_path.getText());
			cd.setMaximaPath(maxima_path.getText());
			try {
				cd.saveConfig();
			}
			catch(CloudDataException e2) {
				JOptionPane jop2 = new JOptionPane();
				jop2.showMessageDialog(null, e2.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	*	Initialise la classe IntroPanel2
	*/
	public void initComponent() {

		this.listelem = new JPanel();
		this.elem_sci = new JPanel();
		this. elem_max = new JPanel();
		this.selem_sci = new JPanel();
		this.selem_max = new JPanel();

		this.intro_txt = new JLabel(this.lang.getString("detect_software"));
		this.scilab_txt = new JLabel("\t" + this.lang.getString("sci"));
		this.maxima_txt = new JLabel("\t" + this.lang.getString("max"));
		this.scilab_path = new JTextField(cd.getScilabPath());
		this.maxima_path = new JTextField(cd.getMaximaPath());

		this.scilab_but = new JButton(this.lang.getString("find_sci"));
		this.maxima_but = new JButton(this.lang.getString("find_max"));
		this.Save = new JButton(this.lang.getString("Save"));

	}

	/**
	*	Réinitialise la classe IntroPanel2 lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.intro_txt.setText(this.lang.getString("detect_software"));
		this.scilab_txt.setText("\t" + this.lang.getString("sci"));
		this.maxima_txt.setText("\t" + this.lang.getString("max"));

		this.scilab_but.setText(this.lang.getString("find_sci"));
		this.maxima_but.setText(this.lang.getString("find_max"));
		this.Save.setText(this.lang.getString("Save"));

	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		
	}

}




