/*	SysEq.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page permettant d'entrer directement les équations système.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import PropertyFile.i18n;

public class SysEq extends JPanel {

	private JLabel entrees;
	private JLabel etats;
	private JLabel eq;
	private JLabel hspace;
	private JTextField Entrees;
	private JTextField Etats;
	private JTextArea Eq;

	private i18n lang;

	private CloudData cd;

	public SysEq(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

			//Texte: "Entrées: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(entrees,c);

			//zone de texte: Entrées
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(Entrees,c);

			//espace fin de 1ere ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(hspace,c);



			//Texte: "Etats: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(etats,c);

			//zone de texte: Etats
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 4;
		this.add(Etats,c);

			//espace fin de 2eme ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 1;
		c.gridwidth = 4;
		this.add(hspace,c);



			//Texte: "Equations: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(eq,c);

			//zone de texte: Equations
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(Eq,c);

			//espace fin de 3eme ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(hspace,c);
	}

	/**
	*	Initialise la classe SysEq
	*/
	public void initComponent() {

		this.entrees = new JLabel(this.lang.getString("inputs"));
		this.etats = new JLabel(this.lang.getString("states"));
		this.eq = new JLabel(this.lang.getString("equations"));
		this.hspace = new JLabel(" ");
		this.Entrees = new JTextField();
		this.Etats = new JTextField();
		this.Eq = new JTextArea(5,1);

	}

	/**
	*	Réinitialise la classe SysEq lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.entrees.setText(this.lang.getString("inputs"));
		this.etats.setText(this.lang.getString("states"));
		this.eq.setText(this.lang.getString("equations"));

	}

	/**
	*	Enregistre les données entrées par l'utilisateur dans la classe CloudData
	* pour pouvoir les traiter et lancer les calculs Scilab ou Maxima
	*/
	public void set() {

		try {
			this.cd.setInputEqus(Etats.getText(),Entrees.getText(), Eq.getText());
		}
		catch (CloudDataException e) {
			JOptionPane jop = new JOptionPane();
			jop.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		Etats.setText(cd.getOriginalStates());
		Entrees.setText(cd.getOriginalInputs());
		Eq.setText(cd.getOriginalSystem());
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		cd.saveInputEqus(Etats.getText(), Entrees.getText(), Eq.getText());
	}
}
