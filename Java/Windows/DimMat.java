/*	DimMat.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fenêtre permettant d'entrer les dimensions d'une matrice pour ouvrir l'assistant d'écriture de matrices.
 */

package Windows;

import Windows.Assistant;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
import static java.lang.Math.*;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JDialog;

import PropertyFile.i18n;

public class DimMat extends JDialog {

	private JPanel panTexte;
	private JLabel lignesLabel, colonnesLabel;
	private JTextField lignes, colonnes;
	private Assistant ass;
	private JTextField mat;

	private i18n lang;

	private CloudData cd;

	public DimMat(JTextField matrice, JFrame parent, String title, boolean modal, i18n fichierlang, CloudData cloud) {
		super(parent, title, modal);
		this.mat = matrice;
		this.setSize(450, 150);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();
	}

	/**
	*	Initialise la classe DimMat
	*/
	private void initComponent(){

		//Texte et zones de texte
		this.panTexte = new JPanel();
		this.panTexte.setBackground(Color.white);
		this.panTexte.setPreferredSize(new Dimension(420, 100));
		this.lignes = new JTextField();
		this.lignes.setPreferredSize(new Dimension(40, 40));
		this.colonnes = new JTextField();
		this.colonnes.setPreferredSize(new Dimension(40, 40));
		this.panTexte.setBorder(BorderFactory.createTitledBorder(this.lang.getString("dims_mat")));
		//lignesLabel = new JLabel("Lignes :");
		this.lignesLabel = new JLabel(this.lang.getString("rows"));
		this.colonnesLabel = new JLabel(this.lang.getString("columns"));
		this.panTexte.add(lignesLabel);
		this.panTexte.add(lignes);
		this.panTexte.add(colonnesLabel);
		this.panTexte.add(colonnes);



		JPanel content = new JPanel();
		content.setBackground(Color.white);
		content.add(panTexte);
		
		JPanel control = new JPanel();
		JButton okBouton = new JButton(this.lang.getString("Ok"));
		
		okBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {				
				ass = new Assistant(Integer.parseInt(lignes.getText()), Integer.parseInt(colonnes.getText()), mat, lang, cd);
				setVisible(false);
			}		
		});
		
		JButton cancelBouton = new JButton(this.lang.getString("Cancel"));
		cancelBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}			
		});
		
		control.add(okBouton);
		control.add(cancelBouton);

		this.getContentPane().add(content, BorderLayout.CENTER);
		this.getContentPane().add(control, BorderLayout.SOUTH);

		this.setVisible(true);

	}
}


