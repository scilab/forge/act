/*	SysH.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page permettant d'entrer le système sous forme d'une fonction de transfert.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JOptionPane;

import PropertyFile.i18n;

public class SysH extends JPanel {

	private JLabel var;
	private JLabel coef;
	private JLabel num;
	private JLabel den;
	private JLabel N;
	private JLabel D;
	private JLabel ex;

	private JLabel hspace;

	private JRadioButton s;
	private JRadioButton p;
	private JRadioButton z;
	private JRadioButton w;
	private ButtonGroup vargroup;
	private String Var;

	private JTextField Coef;
	private JTextField Num;
	private JTextField Den ;

	private i18n lang;

	private CloudData cd;

	public SysH(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();


			//Texte: "Variable: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		this.add(var,c);

			//Cases à cocher de la variable
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 2;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(s,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 3;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(p,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 4;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(z,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(w,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 6;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(hspace,c);


			//Texte: "Coefficient: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(coef,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(hspace,c);

			//Zone de texte: Coef
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 10.0;
		c.weighty = 1.0;
		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 4;
		this.add(Coef,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 6;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(hspace,c);


			//Texte: "Numérateur: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(num,c);

			//Texte: "Numérateur: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(N,c);

			//Zone de texte: Num
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 10.0;
		c.weighty = 1.0;
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(Num,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 6;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(hspace,c);


			//Texte: "Dénominateur: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(den,c);

			//Texte: "Dénominateur: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(D,c);

			//Zone de texte: Den
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 10.0;
		c.weighty = 1.0;
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 4;
		this.add(Den,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 6;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(hspace,c);



			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 2;
		this.add(hspace,c);

			//example de fonction de transfert
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 4;
		c.gridwidth = 3;
		this.add(ex,c);

			//Espace horizontale
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 4;
		c.gridy = 4;
		c.gridwidth = 2;
		this.add(hspace,c);

	}

	/**
	*	Initialise la classe SysH
	*/
	public void initComponent() {

		this.var = new JLabel(this.lang.getString("variable"));
		this.coef = new JLabel(this.lang.getString("static_gain"));
		this.num = new JLabel(this.lang.getString("numerator"));
		this.den = new JLabel(this.lang.getString("denominator"));

		this.hspace = new JLabel(" ");

		this.s = new JRadioButton(this.lang.getString("s_default"));
		this.s.setSelected(true);
		this.p = new JRadioButton("p");
		this.z = new JRadioButton("z");
		this.w = new JRadioButton("w");
		this.vargroup = new ButtonGroup();
		this.Var = new String("s");

		this.Coef = new JTextField();
		this.Num = new JTextField();
		this.Den = new JTextField();

		this.vargroup.add(s);
		this.vargroup.add(p);
		this.vargroup.add(z);
		this.vargroup.add(w);

		this.s.addActionListener(new VarListener());
		this.p.addActionListener(new VarListener());
		this.z.addActionListener(new VarListener());
		this.w.addActionListener(new VarListener());

		this.N = new JLabel("N("+Var+") = ");
		this.D = new JLabel("D("+Var+") = ");
		this.ex = new JLabel("H("+Var+") := K.N("+Var+")/D("+Var+")");
	}

	/**
	*	Réinitialise la classe SysH lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.var.setText(this.lang.getString("variable"));
		this.coef.setText(this.lang.getString("static_gain"));
		this.num.setText(this.lang.getString("numerator"));
		this.den.setText(this.lang.getString("denominator"));

		this.s.setText(this.lang.getString("s_default"));
	}


	public class VarListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			JRadioButton source = (JRadioButton) e.getSource();
			//trucs à faire
			if (source == s)	Var = "s";
			else if (source == p)	Var = "p";
			else if (source == z)	Var = "z";
			else if (source == w)	Var = "w";

			N.setText("N("+Var+") = ");
			D.setText("D("+Var+") = ");
			ex.setText("H("+Var+") := K.N("+Var+")/D("+Var+")");

			repaint();

		}
	}

	/**
	*	Enregistre les données entrées par l'utilisateur dans la classe CloudData
	* pour pouvoir les traiter et lancer les calculs Scilab ou Maxima
	*/
	public void set() {

		try {
			this.cd.setInputH(Var.charAt(0), Coef.getText(), Num.getText(), Den.getText());
		}
		catch (CloudDataException e) {
			JOptionPane jop = new JOptionPane();
			jop.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		Var = ((Character)cd.getOriginalHvariable()).toString();
		Coef.setText(cd.getOriginalHgain());
		Num.setText(cd.getOriginalHnumerator());
		Den.setText(cd.getOriginalHdenominator());
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		cd.saveInputH(Var.charAt(0), Coef.getText(), Num.getText(), Den.getText());
	}
}
