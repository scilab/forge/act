/*	Configuration.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fenêtre de configuration.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
import static java.lang.Math.*;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JDialog;

import PropertyFile.i18n;

public class Configuration extends JDialog {

	private i18n lang;
	private CloudData cd;
	private ConfigGrid grid;

	public Configuration(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();
		this.grid = new ConfigGrid(lang, cd);

		//Définit une taille pour celle-ci ;
		this.setSize(500, 800);

		this.setLayout(new BorderLayout());
		
		JPanel control = new JPanel();

		JButton okBouton = new JButton("Ok");
		okBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {				
				// TODO
				setVisible(false);
			}		
		});
		
		JButton cancelBouton = new JButton(lang.getString("Annuler"));
		cancelBouton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				// TODO
				setVisible(false);
			}			
		});
		
		control.add(okBouton);
		control.add(cancelBouton);

		this.getContentPane().add(grid, BorderLayout.CENTER);
		this.getContentPane().add(control, BorderLayout.SOUTH);




	}



	/**
	*	Initialise la classe Configuration
	*/
	public void initComponent() {

		//Définit un titre pour votre fenêtre
		this.setTitle(this.lang.getString("configuration"));
		//Nous allons maintenant dire à notre objet de se positionner au centre
		this.setLocationRelativeTo(null);
		//On prévient notre JFrame que ce sera notre JPanel qui sera son contentPane
		//this.setContentPane(tab);

		this.setVisible(true);
	}
}







