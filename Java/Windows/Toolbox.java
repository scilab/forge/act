/*	Toolbox.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fichier main.
 */

import Windows.Toolboxmainwindow;
import CloudData.CloudData;
import CloudData.CloudDataException;
import javax.swing.JFrame;
 
public class Toolbox {
 
        public static void main(String[] args){

		CloudData cd = new CloudData();

		cd.setScilabPath("scilab");
		cd.setMaximaPath("maxima");
		cd.seti18nDir("../i18n/");

                Toolboxmainwindow fen = new Toolboxmainwindow(cd);
        }
}





