/*	SysAlpha.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Page permettant d'entrer les coefficients d'Ackerman (Alphas) du système.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import PropertyFile.i18n;

public class SysAlpha extends JPanel {

	private JLabel alphas;
	private JLabel intro1;
	private JLabel intro2;
	private JLabel ex;
	private JLabel hspace;
	private JTextField Alphas;

	private i18n lang;

	private CloudData cd;

	public SysAlpha(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

			//espace début de ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(hspace,c);

			//Texte introduction 1
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 8;
		this.add(intro1,c);





			//espace début de ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(hspace,c);

			//Texte introduction 2
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 8;
		this.add(intro2,c);





			//Texte: "Alphas: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(alphas,c);

			//zone de texte: Alphas
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(Alphas,c);

			//espace fin de ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(hspace,c);





			//espace début de ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(hspace,c);

			//Texte exemple
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 8;
		this.add(ex,c);




			//espace ligne supplémentaire
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 9;
		this.add(hspace,c);
	}

	/**
	*	Initialise la classe SysAlpha
	*/
	public void initComponent() {

		this.alphas = new JLabel(this.lang.getString("alphas"));
		this.intro1 = new JLabel(this.lang.getString("enter_coefs"));
		this.intro2 = new JLabel(this.lang.getString("order"));
		this.ex = new JLabel(this.lang.getString("ex"));
		this.hspace = new JLabel(" ");
		this.Alphas = new JTextField();

	}

	/**
	*	Réinitialise la classe SysAlpha lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.alphas.setText(this.lang.getString("alphas"));
		this.intro1.setText(this.lang.getString("enter_coefs"));
		this.intro2.setText(this.lang.getString("order"));
		this.ex.setText(this.lang.getString("ex"));

	}

	/**
	*	Enregistre les données entrées par l'utilisateur dans la classe CloudData
	* pour pouvoir les traiter et lancer les calculs Scilab ou Maxima
	*/
	public void set() {

		try {
			this.cd.setInputAlpha(Alphas.getText());
		}
		catch (CloudDataException e) {
			JOptionPane jop = new JOptionPane();
			jop.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		Alphas.setText(cd.getOriginalAlpha());
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		cd.saveInputAlpha(Alphas.getText());
	}
}
