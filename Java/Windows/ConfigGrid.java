/*	ConfigGrid.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Eléments centraux de la fenêtre Configuration.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.io.File;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
import static java.lang.Math.*;
import static java.lang.Double.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JFileChooser;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import java.lang.Double.*;

import java.util.ArrayList;

import PropertyFile.i18n;
import SciMatrix.SciMatrix;

public class ConfigGrid extends JPanel {

	private String sci;
	private String max;

	private JPanel listelem;
	private JPanel elem_sci;
	private JPanel elem_max;
	private JPanel selem_sci;
	private JPanel selem_max;

	private JLabel intro_txt;
	private JLabel scilab_txt;
	private JLabel maxima_txt;
	private JLabel scilab_ver;
	private JLabel maxima_ver;
	private JLabel defaultLang;

	private JButton scilab_but;
	private JButton maxima_but;

	private JComboBox box;

	private i18n lang;

	private CloudData cd;

	public ConfigGrid(i18n fichierlang, CloudData cloud) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		// Text : "Détection des logiciels"
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(intro_txt,c);

		// Text	: "Scilab : "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(scilab_txt,c);

		// Text : Version de Scilab
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(scilab_ver,c);

		// Bouton : "Trouver Scilab manuellement"
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 2;
		c.gridwidth = 2;
		this.add(scilab_but,c);

		// Text : "Maxima : "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(maxima_txt,c);	

		// Text : Version de Maxima
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(maxima_ver,c);

		// Bouton : "Trouver Maxima manuellement"
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 4;
		c.gridwidth = 2;
		this.add(maxima_but,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 1;
		c.gridy = 5;
		c.gridwidth = 2;
		this.add(defaultLang,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 100.0;
		c.weighty = 100.0;
		c.gridx = 2;
		c.gridy = 5;
		c.gridwidth = 2;
		this.add(box,c);

		scilab_but.addActionListener(new LangBoxListener());
		maxima_but.addActionListener(new LangBoxListener());
	}

	public class LangBoxListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {

			JButton source = (JButton) e.getSource();

			try{    
			   	JFileChooser chooser = new JFileChooser();
				    
			  	 // Dossier Courant
			 	 chooser.setCurrentDirectory(new  File("."+File.separator)); 
					    
			 	  //Affichage et récupération de la réponse de l'utilisateur
			 	  int reponse = chooser.showDialog(chooser,lang.getString("Path"));
				     
			 	 // Si l'utilisateur clique sur OK
				  if  (reponse == JFileChooser.APPROVE_OPTION){
				
					 // Récupération du chemin du fichier
					String  fichier = chooser.getSelectedFile().toString(); 
				
					 //Ecriture du fichier
					if (source == scilab_but)	cd.setScilabPath(fichier);
					else if (source == maxima_but)	cd.setMaximaPath(fichier);
			 	  }
			}
			catch(HeadlessException he) {
				he.printStackTrace();
				JOptionPane jop = new JOptionPane();
				jop.showMessageDialog(null, he.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
			}
		}
	}

        class ItemAction implements ActionListener{
 
                public void actionPerformed(ActionEvent e) {

			if(box.getSelectedItem() == lang.getString("french_FR")) {
				// TODO
			}
			else if(box.getSelectedItem() == lang.getString("english_EN")) {
				// TODO
			}
			else if(box.getSelectedItem() == lang.getString("german")) {
				// TODO
			}
                }               
        }

	/**
	*	Initialise la classe ConfigGrid
	*/
	public void initComponent() {

		this.listelem = new JPanel();
		this.elem_sci = new JPanel();
		this.elem_max = new JPanel();
		this.selem_sci = new JPanel();
		this.selem_max = new JPanel();

		this.intro_txt = new JLabel(this.lang.getString("detect_software"));
		this.scilab_txt = new JLabel("\t" + this.lang.getString("sci"));
		this.maxima_txt = new JLabel("\t" + this.lang.getString("max"));
		this.scilab_ver = new JLabel(this.lang.getString("v_sci"));
		this.maxima_ver = new JLabel(this.lang.getString("v_max"));

		this.scilab_but = new JButton(this.lang.getString("find_sci"));
		this.maxima_but = new JButton(this.lang.getString("find_max"));

		this.defaultLang = new JLabel(this.lang.getString("defaultLanguage"));
		box.setPreferredSize(new Dimension(300,20));
		box.addActionListener(new ItemAction());
		box.addItem(this.lang.getString("french_FR"));
		box.addItem(this.lang.getString("english_EN"));
		box.addItem(this.lang.getString("german"));
	}

	/**
	*	Réinitialise la classe ConfigGrid lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.intro_txt.setText(this.lang.getString("detect_software"));
		this.scilab_txt.setText("\t" + this.lang.getString("sci"));
		this.maxima_txt.setText("\t" + this.lang.getString("max"));
		this.scilab_ver.setText(this.lang.getString("v_sci"));
		this.maxima_ver.setText(this.lang.getString("v_max"));

		this.scilab_but.setText(this.lang.getString("find_sci"));
		this.maxima_but.setText(this.lang.getString("find_max"));

		this.defaultLang.setText(this.lang.getString("defaultLanguage"));
		box.addItem(this.lang.getString("french_FR"));
		box.addItem(this.lang.getString("english_EN"));
		box.addItem(this.lang.getString("german"));
	}
}

