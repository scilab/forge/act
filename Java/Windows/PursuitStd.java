/*	PursuitStd.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fenêtre permettant d'entrer les équations de sortie désirées pour le système pour ensuite faire un suivi de trajectoire.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;

import PropertyFile.i18n;

public class PursuitStd extends JPanel {

	//private int n = 1;
	//private JLabel s;
	//private JLabel eq;
	//private JTextField[] s_eq;
	private JLabel l, ex, eq;
	private JLabel hspace;
	private JTextField L;
	private JTextArea Eq;
	private JButton bouton;

	private i18n lang;
	private CloudData cd;
	private JCheckBox ch;

	public PursuitStd(i18n fichierlang, CloudData cloud, JCheckBox check) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.ch = check;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();


			//Texte: "Lambdas : "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(l,c);

			//zone de texte: Lambdas
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(L,c);

			//espace fin de 1ere ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(hspace,c);

			//Texte: "ex : "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 5;
		this.add(ex,c);

			//Texte: "Equations: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(eq,c);

			//zone de texte: Equations
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(Eq,c);

			//espace fin de 2ème ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(hspace,c);

			//espaces début de 3ème ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		this.add(hspace,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 4;
		this.add(hspace,c);

			//Bouton Poursuite de trajectoire
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 3;
		c.gridwidth = 4;
		this.add(bouton,c);




/*			//texte Sortie(s)
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(s,c);

			//texte Equation(s)
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 10;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 4;
		this.add(eq,c);
		
			//zones de texte
		int i;
		for(i=0;i<this.n;i++) {
				//s
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1.0;
			c.weighty = 1.0;
			c.gridx = 0;
			c.gridy = i;
			c.gridwidth = 1;
			this.add(s_eq[2*i],c);
		
				//eq
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 10;
			c.weighty = 1.0;
			c.gridx = 1;
			c.gridy = i;
			c.gridwidth = 4;
			this.add(s_eq[2*i+1],c);
		}*/

		bouton.addActionListener(new PursuitListener());

	}



	/**
	*	Initialise la classe PursuitStd
	*/
	public void initComponent() {

		//this.s = new JLabel(this.lang.getString("outputs"));
		//this.eq = new JLabel(this.lang.getString("equations3"));
		//this.s_eq = new JTextField[2*n];
		this.l = new JLabel(this.lang.getString("lambdas"));
		this.ex = new JLabel(this.lang.getString("ex3"));
		this.eq = new JLabel(this.lang.getString("eq_traj_des"));
		this.hspace = new JLabel("    ");
		this.Eq = new JTextArea(10,1);
		this.L = new JTextField();
		this.bouton = new JButton(this.lang.getString("tracking"));
	}

	/**
	*	Réinitialise la classe PursuitStd lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		//this.s.setText(this.lang.getString("outputs"));
		//this.eq.setText(this.lang.getString("equations3"));
		this.l.setText(this.lang.getString("lambdas"));
		this.ex.setText(this.lang.getString("ex3"));
		this.eq.setText(this.lang.getString("eq_traj_des"));
		this.bouton.setText(this.lang.getString("tracking"));
	}


	public class PursuitListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {
			try {
				cd.setPursuitStd(Eq.getText(), L.getText(), ch.isSelected());
				cd.setPursuit();
			}
			catch(CloudDataException e1) {
				JOptionPane jop1 = new JOptionPane();
				jop1.showMessageDialog(null, e1.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
			}
		}
	}



	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		Eq.setText(cd.getOriginalStd());
	}

	/**
	*	Fonction de sauvegarde des données
	*
	* @param ch
	*	Choix de l'utilisateur d'utiliser le système stabilisé ou non
	*/
	public void save() {
		cd.savePursuitStd(Eq.getText(), L.getText(), ch.isSelected());
	}
}


