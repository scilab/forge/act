/*	StabStd.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Fenêtre permettant d'entrer les pôles désirés du système pour ensuite le stabiliser.
 */

package Windows;

import CloudData.CloudData;
import CloudData.CloudDataException;
import Windows.Pursuit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.*;
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import PropertyFile.i18n;

public class StabStd extends JPanel {

	private JLabel poles;
	private JLabel intro;
	private JLabel hspace;
	private JTextField Poles;
	private JButton bouton;

	private Pursuit purs;
	private i18n lang;

	private CloudData cd;

	public StabStd(i18n fichierlang, CloudData cloud, Pursuit pursuit) {

		this.lang = fichierlang;
		this.cd = cloud;
		this.purs = pursuit;
		this.initComponent();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

			//espaces début d'introduction
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		this.add(hspace,c);

			//texte introduction
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 2.0;
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 8;
		this.add(intro,c);

			//Texte: "Pôles: "
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		this.add(poles,c);

			//zone de texte: Pôles
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 4;
		this.add(Poles,c);

			//espace fin de 1ere ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 1;
		c.gridwidth = 4;
		this.add(hspace,c);

			//espaces début de 2eme ligne
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		this.add(hspace,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 20.0;
		c.weighty = 1.0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(hspace,c);

			//Bouton Stabilisation
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 2.0;
		c.weighty = 1.0;
		c.gridx = 5;
		c.gridy = 2;
		c.gridwidth = 4;
		this.add(bouton,c);



			//espaces ligne supplémentaire
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 9;
		this.add(hspace,c);

		bouton.addActionListener(new StabListener());
	}


	/**
	*	Initialise la classe StabStd
	*/
	public void initComponent() {

		this.poles = new JLabel(this.lang.getString("poles"));
		this.intro = new JLabel(this.lang.getString("enter_poles2"));
		this.hspace = new JLabel(" ");
		this.Poles = new JTextField();
		this.bouton = new JButton(this.lang.getString("stabilization"));
	}

	/**
	*	Réinitialise la classe StabStd lors d'un changement de fichier de lague
	*
	* @param fichierlang
	*	Nouveau fichier de langue .i18n
	*/
	public void reinitComponent(i18n fichierlang) {

		this.lang = fichierlang;

		this.poles.setText(this.lang.getString("poles"));
		this.intro.setText(this.lang.getString("enter_poles2"));
		this.bouton.setText(this.lang.getString("stabilization"));
	}


	public class StabListener implements ActionListener {

		public void actionPerformed (ActionEvent e) {
			try {
				if(cd.getStabMethod() == 2) cd.setStabAckerman(Poles.getText());
				else if(cd.getStabMethod() == 3) cd.setStabBrunovsky(Poles.getText());
				cd.setStab();
				while(!cd.isStabFinished());
				cd.getStabOutput();

				DispMats dispMats = new DispMats(null, lang.getString("dispMats"), true, cd);

				purs.checkEnabled();
			}
			catch(CloudDataException e1) {
				JOptionPane jop1 = new JOptionPane();
				jop1.showMessageDialog(null, e1.getMessage(), lang.getString("error"), JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	*	Fonction d'ouverture dedonnées enregistrées
	*/
	public void open() {
		Poles.setText(cd.getOriginalAckerman());
	}

	/**
	*	Fonction de sauvegarde des données
	*/
	public void save() {
		cd.saveStabAckerman(Poles.getText());
	}
}

