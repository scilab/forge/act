/*	SciFilter.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Filtre pour les dossiers Scilab.
 */
package Search;

import java.io.File;
import java.io.FilenameFilter;

/** Filter for scilab path.
 */
public class SciFilter implements FilenameFilter
{
	public boolean accept(File dir, String name)
	{
		if (name.substring(0,8) == "scilab-")
			return true;
		else
			return false;
	}
}






