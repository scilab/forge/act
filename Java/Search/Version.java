/*	Version.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Gestion de la version des logiciels.
 */

package Search;

/** Handles softwares version.
 * (with de forme : a.b.c)
 */
public class Version
{
	private int a;
	private int b;
	private int c;	

	/** Initializes a newly created Version object.
	 *
	 *	@param a major number.
	 *	@param b minor number.
	 *	@param c subversion number.
	 */
	public Version(int a, int b, int c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
	}

	/** Initializes a newly created Version object.
	 *
	 *	@param a major number.
	 *	@param b minor number.
	 */
	public Version(int a, int b)
	{
		this(a,b,-1);
	}

	/** Initializes a newly created Version object.
	 *
	 *	@param a major number.
	 */
	public Version(int a)
	{
		this(a,-1,-1);
	}

	/** Initializes a empty Version object.
	 *
	 */
	public Version()
	{
		this(-1,-1,-1);
	}

	/** Gets the major version number.
	 *
	 *	@return the major version number.
	 */
	public int getA()
	{
		return this.a;
	}

	/** Gets the minor version number.
	 *
	 *	@return the minor version number.
	 */
	public int getB()
	{
		return this.b;
	}

	/** Gets the subversion version number.
	 *
	 *	@return the subversion number.
	 */
	public int getC()
	{
		return this.c;
	}

	/** Sets the major version number.
	 *
	 *	@param a the major version number.
	 */
	public void setA(int a)
	{
		this.a = a;
	}

	/** Sets the minor version number.
	 *
	 *	@param a the minor version number.
	 */
	public void setB(int b)
	{
		this.b = b;
	}

	/** Sets the subversion number.
	 *
	 *	@param a the subversion number.
	 */
	public void setC(int c)
	{
		this.c = c;
	}

	/** Compares two version.
	 *
	 *	@param o version to compare with this version.
	 *
	 *	@return 0 if the both are equals, 1 if o is greater than this
	 *		version and -1 if this version if greater than o version.
	 */
	public int comp(Version o)
	{
		if(o.getA() == this.a)
		{
			if(o.getB() == this.b)
			{
				if(o.getC() == this.c)
				{
					return 0;
				}
				else if(o.getC() > this.c)
				{
					return 1;
				}
				else
				{
					return -1;
				}
			}
			else if (o.getB() > this.b)
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
		else if (o.getA() > this.a)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	/** Returns a string representation of a Version object.
	 *
	 *	@param sep separator of major, minor, en subversion number.
	 *
	 *	@return string representation of the object.
	 */
	public String toString(String sep)
	{
		String ret = "";
		if (this.a >= 0)
		{
			ret += this.a;
			if (this.b >= 0)
			{
				ret += sep + this.b;
				if (this.c >= 0)
				{
					ret += sep + this.c;
				}
			}
		}
		else
		{
			ret = "-1";
		}
		return ret;
	}

	/** Returns a string representation of a Version object (with '.' as
	 * default separator).
	 *
	 *	@return string representation of the object.
	 */
	public String toString()
	{
		return this.toString(".");
	}
	
}




