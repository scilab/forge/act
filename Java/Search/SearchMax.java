/*	SearchMax.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Cherche la version de Maxima la plus récente.
 */

package Search;

import java.io.File;

/** Finds the greater version of Maxima on the system.
 *
 */
public class SearchMax
{
	File dir;
	String tdir;
	Version vers;

	/** Initializes the SearchMax object with a search path.
	 *
	 *	@param dir base directory (for the search).
	 */
	public SearchMax(String dir)
	{
		this.dir = new File(dir);
		this.tdir = dir;
	}

	/** Parses the Maxima version from the directory name.
	 *
	 *	@param dir directory name.
	 *
	 *	@return Version object.
	 */
	public static Version parseVersion(String dir)
	{
		String ta = "";
		int a = -1;
		String tb = "";
		int b = -1;
		String tc = "";
		int c = -1;
		dir = dir.substring(7);
		int l = dir.length();
		int i;
		for(i = 0; i < l; i++)
		{
			if (dir.charAt(i) != '.')
			{
				ta += dir.charAt(i);
			}
			else
			{
				break;
			}
		} 
		i++;
		for(;i < l; i++)
		{
			if (dir.charAt(i) != '.')
			{
				tb += dir.charAt(i);
			}
			else
			{
				break;
			}
		}
		i++;
		for(;i < l; i++)
		{
			if (dir.charAt(i) != '.')
			{
				tc += dir.charAt(i);
			}
			else
			{
				break;
			}
		}
		if (ta.length() > 0)
		{
			a = Integer.parseInt(ta.trim());
			if (tb.length() > 0)
			{
				b = Integer.parseInt(tb.trim());
				if (tc.length() > 0)
				{
					c = Integer.parseInt(tc.trim());
				}
			}
		}
		return new Version(a,b,c);
	}

	/** Search the last version of Maxima on a Windows© system.
	 *
	 *	@return Version best version found.
	 */
	public Version searchWin()
	{
		String list[] = dir.list(new MaxFilter());
		Version max = new Version();
		for(String s:list)
		{
			Version v = this.parseVersion(s);
			if (max.comp(v) == 1)
			{
				max = v;
			}
		}
		return max;
	}

	/** Search the last version of Scilab on a Unix system.
	 *
	 *	@return Version best version found.
	 */
	public Version searchUnix()
	{
		try
		{
			Runtime.getRuntime().exec("bash vmax.linux");
		}
		catch(java.io.IOException e)
		{
			e.printStackTrace();
		}
		java.io.BufferedReader br = null;
		File out = new File("maxvers.tmp");
		File end = new File("maxvers.end");
		String line = "";

		while(!end.exists());

		try
		{
			br = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(out),"UTF-8"));
		}
		catch (java.io.FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SecurityException e)
		{
			e.printStackTrace();
		}
		catch (java.io.UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		try
		{
			line = br.readLine();
			if (line == null)
			{
				out.delete();
				end.delete();
				return new Version();
			}
		}
		catch(java.io.IOException e)
		{
			e.printStackTrace();
		}
		//line = line.replace(" ", "-");
		
		Version v = this.parseVersion(line);
		out.delete();
		end.delete();

		return v;
	}

	/** Gets the path of maxima exe (on Windows©).
	 *
	 *	@return string containing de path of the scilab exe.
	 */
	public String pathWin()
	{
		return this.tdir + "Maxima-" + this.vers + "\\maxima.exe";
	}

}







