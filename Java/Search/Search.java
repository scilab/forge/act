/*	SearchMax.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Cherche la version de Maxima la plus récente.
 */

package Search;


public class Search
{
	private Version scivers = null;
	private Version maxvers = null;

	private String scipath = "";
	private String maxpath = "";

	public Search()
	{
		String os = System.getProperty("os.name");
		SearchSci sci = null;
		SearchMax max = null;
		Version vsci = null;
		Version vmax = null;

		System.out.println(os);	// ----------

		if (os.length() >= 7)
		{
			System.out.println("..." + os.substring(0,7) + "..."); // ---------
			if (os.substring(0,7) == "Windows")
			{
				System.out.println("Detection windows :");
				SearchSci sci32 = null;
				Version vsci32 = null;
				SearchMax max32 = null;
				Version vmax32 = null;

				try
				{
					sci = new SearchSci("C:\\Program Files\\");
					vsci = sci.searchWin();
				}
				catch(Exception e)
				{
					vsci = new Version();
				}

				try
				{
					sci32 = new SearchSci("C:\\Program Files (x86)\\");
					vsci32 = sci32.searchWin();
				}
				catch(Exception e)
				{
					vsci32 = new Version();
				}

				try
				{
					max = new SearchMax("C:\\Program Files\\");
					vmax = max.searchWin();
				}
				catch(Exception e)
				{
					vmax = new Version();
				}

				try
				{
					max32 = new SearchMax("C:\\Program Files\\");
					vmax32 = max32.searchWin();
				}
				catch(Exception e)
				{
					vmax32 = new Version();
				}

				if (vsci.comp(vsci32) == 1)
				{
					vsci = vsci32;
					sci = sci32;
				}
				if (vmax.comp(vmax32) == 1)
				{
					vmax = vmax32;
					sci = sci32;
				}
				this.scivers = vsci;
				this.maxvers = vmax;

				if (this.scivers.comp(new Version()) < 0)
				{
					this.scipath = sci.pathWin();
				}
				else
				{
					this.scipath = "";
				}

				if (this.maxvers.comp(new Version()) < 0)
				{
					this.maxpath = max.pathWin();;
				}
				else
				{
					this.maxpath = "";
				}

				//return;
			}
		}
		else
		{

			System.out.println("Detection linux :");
			sci = new SearchSci("./");
			max = new SearchMax("./");

			this.scivers = sci.searchUnix();
			this.maxvers = max.searchUnix();

			if (this.scivers.comp(new Version()) < 0)
			{
				this.scipath = "scilab";
			}
			else
			{
				this.scipath = "";
			}

			if (this.maxvers.comp(new Version()) < 0)
			{
				this.maxpath = "maxima";
			}
			else
			{
				this.maxpath = "";
			}
		}

	}

	public Version getScilabVersion()
	{
		return this.scivers;
	}

	public Version getMaximaVersion()
	{
		return this.maxvers;
	}

	public String getScilabPath()
	{
		return this.scipath;
	}

	public String getMaximaPath()
	{
		return this.maxpath;
	}
}



