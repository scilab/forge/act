/*	TransferFunctionException.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Permet de définir une fonction de transfert par une variable, un
 * gain statique, un numérateur et un dénominateur.
 */

package TransferFunction;

public class TransferFunctionException extends Exception
{
	/** unknow error. */
	public final static int E_UNKNOW = 0;

	/** [TransferFunction] Gain parse error */
	public final static int E_PARSE_G = 1;

	/** [TransferFunction] Monomial value parse error in the numerator */
	public final static int E_NUM_M_PARSE_N = 11;
	/** [TransferFunction] '^' symbol expected afer one variable in the numerator */
	public final static int E_NUM_M_POWNEED = 12;
	/** [TransferFunction] bracket expected in the numerator */
	public final static int E_NUM_M_BRACKET = 13;
	/** [TransferFunction] the numerator is empty */
	public final static int E_NUM_M_EMPTY   = 14;
	
	/** [TransferFunction] the numerator string is empty */
	public final static int E_NUM_P_EMPTY_P = 21;
	/** [TransferFunction] Monomial list in the numerator is empty */
	public final static int E_NUM_P_EMPTY   = 22;
	/** [TransferFunction] the numerator must not have positive and negative powers in the same time */
	public final static int E_NUM_P_SIGN    = 23;


	/** [TransferFunction] Monomial value parse error in the denominator */
	public final static int E_DEN_M_PARSE_N = 31;
	/** [TransferFunction] '^' symbol expected afer one variable in the denominator */
	public final static int E_DEN_M_POWNEED = 32;
	/** [TransferFunction] bracket expected in the denominator */
	public final static int E_DEN_M_BRACKET = 33;
	/** [TransferFunction] the denominator is empty */
	public final static int E_DEN_M_EMPTY   = 34;
	
	/** [TransferFunction] the denominator string is empty */
	public final static int E_DEN_P_EMPTY_P = 41;
	/** [TransferFunction] Monomial list in the denominator is empty */
	public final static int E_DEN_P_EMPTY   = 42;
	/** [TransferFunction] the denominator must not have positive and negative powers in the same time */
	public final static int E_DEN_P_SIGN	= 43;

	private int code = 0;
	private String msg = "";

	/** Constructs a new exception with the specified code and detail message.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 */
	public TransferFunctionException(int code, String msg)
	{
		super();
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception without detail message.
	 *
	 */
	public TransferFunctionException()
	{
		this(0,"");
	}

	/** Constructs a new exception with the specified code, detail message and cause.
	 *
	 * @param code error code.
	 * @param msg detail message.
	 * @param cause error cause.
	 */
	public TransferFunctionException(int code, String msg, Throwable cause)
	{
		super(cause);
		this.code = code;
		this.msg = msg;
	}

	/** Constructs a new exception with the specified cause.
	 *
	 * @param cause error cause.
	 */
	public TransferFunctionException(Throwable cause)
	{
		this(0,"", cause);
	}

	/** Gets the error code.
	 *
	 * @return error code.
	 */
	public int getCode()
	{
		return this.code;
	}

	/** Gets the detail message.
	 *
	 * @return detail message.
	 */
	public String getMsg()
	{
		return this.msg;
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @return error description.
	 */
	public String getMessageFromCode()
	{
		return this.getMessageFromCode(this.code);
	}

	/** Returns an error description corresponding to a specific code.
	 *
	 * @param code error code.
	 *
	 * @return error description.
	 */
	public static String getMessageFromCode(int code)
	{
		switch(code)
		{
			case TransferFunctionException.E_PARSE_G:
				return "[TransferFunction] Gain parse error";
			case TransferFunctionException.E_NUM_M_PARSE_N:
				return "[TransferFunction] Monomial value parse error in the numerator";
			case TransferFunctionException.E_NUM_M_POWNEED:
				return "[TransferFunction] '^' symbol expected afer one variable in the numerator";
			case TransferFunctionException.E_NUM_M_BRACKET:
				return "[TransferFunction] bracket expected in the numerator";
			case TransferFunctionException.E_NUM_M_EMPTY:
				return "[TransferFunction] the numerator is empty";
			case TransferFunctionException.E_NUM_P_EMPTY_P:
				return "[TransferFunction] the numerator string is empty";
			case TransferFunctionException.E_NUM_P_EMPTY:
				return "[TransferFunction] Monomial list in the numerator is empty";
			case TransferFunctionException.E_NUM_P_SIGN:
				return "[TransferFunction] the numerator must not have positive and negative powers in the same time";
			case TransferFunctionException.E_DEN_M_PARSE_N:
				return "[TransferFunction] Monomial value parse error in the denominator";
			case TransferFunctionException.E_DEN_M_POWNEED:
				return "[TransferFunction] '^' symbol expected afer one variable in the denominator";
			case TransferFunctionException.E_DEN_M_BRACKET:
				return "[TransferFunction] bracket expected in the denominator";
			case TransferFunctionException.E_DEN_M_EMPTY:
				return "[TransferFunction] the denominator is empty";
			case TransferFunctionException.E_DEN_P_EMPTY_P:
				return "[TransferFunction] the denominator string is empty";
			case TransferFunctionException.E_DEN_P_EMPTY:
				return "[TransferFunction] Monomial list in the denominator is empty";
			case TransferFunctionException.E_DEN_P_SIGN :
				return "[TransferFunction] the denominator must not have positive and negative powers in the same time";
		}
		return "[TransferFunction] Unknow error";
	}

	/** Returns the error message.
	 *
	 * @return string containing the error description (message).
	 */
	public String getMessage()
	{
		String tmp = "Error " + this.code + " : " + this.getMessageFromCode();
		if ( (this.msg != null) && (this.msg != "") )
			tmp += " (\"" + this.msg + "\").";

		return tmp;
	}


}


