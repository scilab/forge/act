/*	TransferFunction.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Allows to define a transfer function by a variable, a static gain, a
 * numerator and a denominator.
 */

package TransferFunction;

import Polynomial.Polynomial;
import Polynomial.PolynomialException;

/** Allows to define a transfer function by a variable, a static gain, a
 * numerator and a denominator.
 */
public class TransferFunction
{

	private char var = 'x';

	private double gain = 0.0;

	private Polynomial num;
	private Polynomial den;
	
	/** Initializes a newly created TransferFunction object with user
	 * defined variable and gain but with empty numerator and denominator.
	 */
	public TransferFunction(char var, double gain)
	{
		this.var = var;
		this.gain = gain;
		this.num = null;
		this.den = null;
	}

	/** Initializes a newly created TransferFunction object with a
	 * null gain, empty numerator and empty denominator but with an user
	 * defined variable.
	 */
	public TransferFunction(char var)
	{
		this(var,0.0);
	}

	/** Initializes a newly created TransferFunction object with a 'x'
	 * variable, null gain, empty numerator and empty denominator.
	 */
	public TransferFunction()
	{
		this('x');
	}

	/** Initializes a newly created TransferFunction object with string 
	 * objects containing different elements of the transfer function into 
	 * TransferFunction object.
	 *
	 * @param var transfer function variable.
	 * @param gain string representation of gain value.
	 * @param num string representation of the numerator.
	 * @param den string representation of the denominator.
	 *
	 * @throws TransferFunctionException if an error occur.
	 */
	public TransferFunction(char var, String gain, String num, String den) throws TransferFunctionException
	{
		this(var);
		this.fromString(var, gain, num, den);
	}

	/** Converts string objects containing different elements of the
	 * transfer function into TransferFunction object.
	 *
	 * @param var transfer function variable.
	 * @param gain string representation of gain value.
	 * @param num string representation of the numerator.
	 * @param den string representation of the denominator.
	 *
	 * @throws TransferFunctionException if an error occur.
	 */
	public void fromString(char var, String gain, String num, String den) throws TransferFunctionException
	{
		this.var = var;
		try
		{
			this.gain = Double.parseDouble(gain);
		}
		catch(NumberFormatException e)
		{
			this.num = null;
			this.den = null;
			// Erreur sur la lecture du gain
			throw new TransferFunctionException(TransferFunctionException.E_PARSE_G, gain, e);
		}

		try
		{
			this.num = new Polynomial(num, this.var);
		}
		catch(PolynomialException e)
		{
			this.num = null;
			this.den = null;
			throw new TransferFunctionException(e.getCode()+10,num,e);
		}

		try
		{
			this.den = new Polynomial(den, this.var);
		}
		catch(PolynomialException e)
		{
			this.num = null;
			this.den = null;
			throw new TransferFunctionException(e.getCode()+30,den,e);
		}

		this.num.simplify();
		this.num.sort();
		this.den.simplify();
		this.den.sort();
	}

	/** Gets the numerator (Polynomial object).
	 *
	 * @return Polynomial object containing the numerator.
	 */
	public Polynomial getNumerator()
	{
		return this.num;
	}

	/** Gets the denominator (Polynomial object).
	 *
	 * @return Polynomial object containing the denominator.
	 */
	public Polynomial getDenominator()
	{
		return this.den;
	}

	/** Gets a string representation of the numerator.
	 *
	 * @return string representation of the numerator.
	 */
	public String getStringNumerator()
	{
		return this.num.toString(this.var);
	}

	/** Gets the coefficient list of the numerator.
	 *
	 * @return string object containing the coefficient list.
	 *
	 * @throws TransferFunctionException if numerator powers has different
	 * 	signs or if the numerator is empty.
	 */
	public String getCoefNumerator() throws TransferFunctionException
	{
		try
		{
			return this.num.coefListFromZero();
		}
		catch(PolynomialException e)
		{
			throw new TransferFunctionException(e.getCode()+10,e.getMsg(),e);
		}
	}

	/** Gets a string representation of the denominator.
	 *
	 * @return string representation of the denominator.
	 */
	public String getStringDenominator()
	{
		return this.den.toString(this.var);
	}

	/** Gets the coefficient list of the denominator.
	 *
	 * @return string object containing the coefficient list.
	 *
	 * @throws TransferFunctionException if denominator powers has different
	 * 	signs or if the denominator is empty.
	 */
	public String getCoefDenominator() throws TransferFunctionException
	{
		try
		{
			return this.den.coefListFromZero();
		}
		catch(PolynomialException e)
		{
			throw new TransferFunctionException(e.getCode()+30,e.getMsg(),e);
		}
	}

	/** Returns the transfer function variable.
	 *
	 * @return transfer function variable.
	 */
	public char getVar()
	{
		return this.var;
	}

	/** Returns a string object representing the transfer function variable.
	 *
	 * @return transfer function variable.
	 */
	public String getStringVar()
	{
		return "" + this.var;
	}

	/** Returns the gain value.
	 *
	 * @return gain value.
	 */
	public double getGain()
	{
		return this.gain;
	}

	/** Returns a string object representing the transfer function gain.
	 *
	 * @return string object representing the transfer function gain.
	 */
	public String getStringGain()
	{
		return ((Double)this.gain).toString();
	}

	/** Returns a string object representing this transfer function.
	 *
	 * @return string object representing this transfer function.
	 */
	public String toString()
	{
		String ret = "";

		ret += "Variable : " + this.var + "\n";
		ret += "Gain : " + this.gain + "\n";
		ret += "Numerator : " + this.getStringNumerator() + "\n";
		ret += "Denominator : " + this.getStringDenominator();

		return ret;
	}


}



