
import java.util.Locale;
import PropertyFile.i18n;
import PropertyFile.PropertyFileException;

public class tstlang
{
	static public void main(String[] args)
	{
		try
		{
		i18n p = new i18n("../",new Locale("fr","FR"),"lang","i18n");
		System.out.println(p);
		p.save("plop.txt");

		java.util.ArrayList<String> e = new java.util.ArrayList<String>();
		e.add("var1");e.add("var2");e.add("var3");
		
		System.out.println(p.getString("tstvar",e));

		System.out.println(p.getString("tstvar2", "avec"));

		System.out.println(p.getString("tstvar2", "avec", "plop"));
		}
		catch(PropertyFileException e)
		{
			e.printStackTrace();
		}
	}
}
