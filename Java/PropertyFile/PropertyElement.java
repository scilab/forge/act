/*	PropertyElement.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Définit un élément atomique (<nom> = <valeur>) d'un PropertyFile.
 */

package PropertyFile;

/** Allows to define an atomic element (<name> = <value>) of a PropertyFile.
 */
public class PropertyElement
{
	private String name;
	private String value;

	/** Initializes a newly created PropertyElement object with an user 
	 * defined name and value.
	 *
	 * @param name property name.
	 * @param value property value.
	 */
	public PropertyElement(String name, String value)
	{
		this.name = name;
		this.value = value;
	}

	/** Initializes a newly created PropertyElement object with an user
	 * defined name (but without value).
	 *
	 * @param name property name.
	 */
	public PropertyElement(String name)
	{
		this.name = name;
		this.value = null;
	}

	/** Initializes a newly created PropertyElement object with null values.
	 *
	 */
	public PropertyElement()
	{
		this(null,null);
	}

	/** Gets the property name.
	 *
	 * @return property name.
	 */
	public String getName()
	{
		return this.name;
	}

	/** Gets the property value.
	 *
	 * @return property value.
	 */
	public String getValue()
	{
		return this.value;
	}

	/** Sets the property name.
	 *
	 * @param new property name.
	 */
	void setName(String name)
	{
		this.name = name;
	}

	/** Sets the property value.
	 *
	 * @param new property value.
	 */
	void setValue(String value)
	{
		this.value = value;
	}
	
}
