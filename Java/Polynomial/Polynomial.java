/*	Polynomial.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Allows to define a polynomial. Powers can be positive or negative.
 */

package Polynomial;

/** Allows to define a polynomial. Powers can be positive or negative.
 */
public class Polynomial
{
	/** List of monomials */
	private java.util.ArrayList<PNome> monomials;

	/** Variable character */
	private char var = 'x';

	/** Allows to know if the polynomial was simplified.
	 * Here, simplified means to group monomials with the same power
	 * together.
	 */
	private boolean simplified = false;
	/** Allows to know if the polynomial was sorted.
	 * If the polynomial is sorted, the first element is the monomial with
	 * the smallest power and the last element is the monomial with the
	 * highest power.
	 */
	private boolean sorted = false;
	
	/** Initializes a newly created polynomial object.
	 *
	 * @param var polynomial variable.
	 */
	public Polynomial(char var)
	{
		this.var = var;
		this.simplified = false;
		this.sorted = false;
		this.monomials = new java.util.ArrayList<PNome>();
	}

	/** Initializes a newly created polynomial object with 'x' as polynomial
	 * variable.
	 */
	public Polynomial()
	{
		this('x');
	}

	/** Initializes a newly created polynomial object with a string containing
	 * the polynomial representation.
	 *
	 * @param poly string containing the polynomial representation to be parsed.
	 * @param var user defined polynomial variable.
	 *
	 * @throws PolynomialException if the string does not contain a parsable
	 *	polynomial.
	 */
	public Polynomial(String poly, char var) throws PolynomialException
	{
		this(var);
		this.fromString(poly,var);
	}

	/** Parses the string argument as polynomial (in the current object).
	 *
	 * @param poly string containing the polynomial representation to be parsed.
	 *
	 * @throws PolynomialException if the string does not contain a parsable
	 *	polynomial.
	 */
	public void fromString(String poly) throws PolynomialException
	{
		this.fromString(poly, this.var);
	}

	/** Parses the string argument as polynomial (in the current object).
	 *
	 * @param poly string containing the polynomial representation to be parsed.
	 * @param var user defined polynomial variable.
	 *
	 * @throws PolynomialException if the string does not contain a parsable
	 *	polynomial.
	 */
	public void fromString(String poly, char var) throws PolynomialException
	{
		String tmp = "";
		int l = poly.length();
		char tc = 0;
		this.monomials.clear();
		for(int i = 0; i < l; i++)
		{
			tc = poly.charAt(i);
			if(tc > 32)	// Suppression de tout les caractère spéciaux
			{
				if( (tc == '+') || (tc == '-') ) // Séparateur
				{
					if(tmp.length() >= 2) // Si il est possible que le signe soit dans la puissance (pas un séparateur)
					{
						// Si le dernier caractère de tmp est '^' ou si les deux derniers sont "^(" le signe n'est pas un séparateur.
						if( (tmp.charAt(tmp.length()-1) == '^') || ( (tmp.charAt(tmp.length()-1) == '(') && (tmp.charAt(tmp.length()-2) == '^')) )
							tmp += tc;
						else
						{
							this.monomials.add(new PNome(tmp,var));
							tmp = "" + tc;
						}
					}
					else
					{
						if(tmp.length() > 0)
							this.monomials.add(new PNome(tmp,var));
						else if (this.monomials.size() > 0)
						{
							// Erreur : La chaine est vide
							throw new PolynomialException(PolynomialException.E_M_EMPTY,null);
						}
						tmp = "" + tc;
					}
				}
				else
				{
					tmp += tc;
				}
			}
		}
		// On enregistre le monôme
		if(tmp.length() > 0)
		{
			this.monomials.add(new PNome(tmp,var));
		}
		else
		{
			// La chaine du polynôme est vide.
			throw new PolynomialException(PolynomialException.E_P_EMPTY_P, null);
		}
	}

	/** Groups monomials with the same power.
	 *
	 */
	public void simplify()
	{
		// On créer une nouvelle liste temporaire
		java.util.ArrayList<PNome> tnomes = new java.util.ArrayList<PNome>();
		PNome tmp;
		PNome rec;
		while(!this.monomials.isEmpty())
		{
			tmp = this.monomials.get(0);
			// On met le premier élément dans la liste temporaire
			tmp = new PNome(tmp.getA(), tmp.getPow());
			// Et on le supprime de la liste original
			this.monomials.remove(0);
			// On parcourt la liste
			for(int i = 0; i < this.monomials.size() ; i++)
			{
				rec = this.monomials.get(i);
				// si l'élément actuelle à la même puissance que tmp
				if(rec.getPow() == tmp.getPow())
				{
					tmp.setA(tmp.getA()+rec.getA());
					this.monomials.remove(i);
					i--;
				}
			}
			tnomes.add(tmp);
		}
		this.monomials = tnomes;
		this.simplified = true;
	}

	/** Sorts the polynomial by power. The first element is the lowest power.
	 *
	 */
	public void sort()
	{
		java.util.Collections.sort(this.monomials);
		this.sorted = true;
	}

	/** Returns true if the polynomial il already simplified and else false.
	 *
	 * @return Return true if the polynomial il already simplified and else false.
	 */
	public boolean isSimplified()
	{
		return this.simplified;
	}

	/** Returns true if the polynomial il already sorted and else false.
	 *
	 * @return Return true if the polynomial il already sorted and else false.
	 */
	public boolean isSorted()
	{
		return this.sorted;
	}

	/** Gets the minimum power of the monomial list.
	 *
	 * @return value of the minimum power in the polynomial.
	 *
	 * @throws PolynomialException if the list is empty.
	 */
	public int getMinGeneric() throws PolynomialException
	{
		if (this.monomials.size() == 0)
		{
			// La liste des monômes est vide.
			throw new PolynomialException(PolynomialException.E_P_EMPTY,null);
		}
		PNome tmp = this.monomials.get(0);
		int min = tmp.getPow();
		// On parcourt la liste
		for(int i = 1; i < this.monomials.size(); i++)
		{
			tmp = this.monomials.get(i);
			if(tmp.getPow() < min)		// Si la puissance courante est la plus petite,
				min = tmp.getPow();	// On redéfinit la plus petite valeur trouvé.
		}
		return min;
	}

	/** Gets the minimum power of the monomial list (with optimization if
	 * the list is already sorted).
	 *
	 * @return value of the minimum power in the polynomial.
	 *
	 * @throws PolynomialException if the list is empty.
	 */
	public int getMin() throws PolynomialException
	{
		if(this.sorted)	// Si la liste est triée, on récupère la puissance du 1er élément.
		{
			if (this.monomials.size() == 0)
			{
				// La liste des monômes est vide.
				throw new PolynomialException(PolynomialException.E_P_EMPTY,null);
			}
			return this.monomials.get(0).getPow();
		}
		return this.getMinGeneric();
	}

	/** Gets the maximum power of the monomial list.
	 *
	 * @return value of the maximum power in the polynomial.
	 *
	 * @throws PolynomialException if the list is empty.
	 */
	public int getMaxGeneric() throws PolynomialException
	{
		if (this.monomials.size() == 0)
		{
			// La liste des monômes est vide.
			throw new PolynomialException(PolynomialException.E_P_EMPTY,null);
		}
		PNome tmp = this.monomials.get(0);
		int max = tmp.getPow();
		// On parcourt la liste
		for(int i = 1; i < this.monomials.size(); i++)
		{
			tmp = this.monomials.get(i);
			if(tmp.getPow() > max)		// Si la puissance courante est la plus grande,
				max = tmp.getPow();	// On redéfinit la plus petite valeur trouvé.
		}
		return max;
	}

	/** Gets the maximum power of the monomial list (with optimization if
	 * the list is already sorted).
	 *
	 * @return value of the maximum power in the polynomial.
	 *
	 * @throws PolynomialException if the list is empty.
	 */
	public int getMax() throws PolynomialException
	{
		if(this.sorted) // Si la liste est triée, on récupère le dernier élément.
		{
			if (this.monomials.size() == 0)
			{
				// La liste des monômes est vide.
				throw new PolynomialException(PolynomialException.E_P_EMPTY,null);
			}
			return this.monomials.get(this.monomials.size()-1).getPow();
		}
		return this.getMinGeneric();
	}

	/** Gets the monomial at position i.
	 *
 	 * @return PNome object that represent the monomial.
	 */
	public PNome get(int i)
	{
		return this.monomials.get(i);
	}

	/** Gets the polynomial size (in number of monomials).
	 *
	 * @return return the polynomial size (in number of monomials).
	 */
	public int size()
	{
		return this.monomials.size();
	}

	/** Gets a string that represent a scilab vector containing the list
	 * of coefficients from the min power to the max power. The list must
	 * be sorted and simplified. 
	 *
	 * @return a string that represent the coefficient of the polynomial in
	 *	a scilab vector. If the list is not sorted or simplified this
	 *	function returns a 'null' value.
	 *
	 * @throws PolynomialException if the list is empty.
	 */
	public String coefList() throws PolynomialException
	{
		if (this.monomials.size() == 0)
		{
			return "";
		}

		if(!(this.simplified && this.sorted))
			return null;

		String ret = "";
		int pow = this.getMin();
		PNome tmp = null;
		// On parcourt tous les monômes
		for(int i = 0; i < this.monomials.size();i++)
		{
			tmp = this.monomials.get(i);
			while(pow < tmp.getPow())	// Si la puissance courant est plus petite
			{
				ret += "0,";		// On remplit de zéro.
				pow++;
			}
			ret += tmp.getA() + ",";	// Sinon on insère la valeur.
			pow ++;
		}
		ret = ret.substring(0,ret.length()-1);	// On supprime la dernière virgule.
		return ret;
	}

	/** Gets a string that represent a scilab vector containing the list
	 * of coefficients. The polynomial must be positive (all the powers >= 0)
	 * or negative (all the powers <= 0). Moreover the list must be sorted
	 * and simplified.
	 *
	 * @return polynomial coefficient.
	 */
	public String coefListFromZero() throws PolynomialException
	{
		if(this.monomials.size() == 0)
			return "";

		if(!(this.simplified && this.sorted))
			return null;

		int min = this.getMin();
		int max = this.getMax();
		
		String ret = "";

		if( (min >= 0) && (max >= 0) )
		{
			for(int i = 0; i < min ; i++)
				ret += "0,";
			ret += this.coefList();
		}
		else if( (min <= 0) && (max <= 0) )
		{
			ret += this.coefList();
			for(int i = max; i < 0; i++)
				ret += ",0";
		}
		else
			throw new PolynomialException(PolynomialException.E_P_SIGN,"");
		return ret;
	}

	/** Returns a string object representing this polynomial.
	 *
	 * @return a string representation of the value of this polynomial.
	 */
	public String toString()
	{
		return this.toString(this.var);
	}

	/** Returns a string object representing this polynomial with an user
	 * defined variable.
	 *
	 * @param var user defined variable.
	 *
	 * @return a string representation of the value of this polynomial.
	 */
	public String toString(char var)
	{
		String ret = "";
		PNome tmp;
		double ta;
		int l = this.monomials.size();
		for(int i = 0; i < l; i++)
		{
			tmp = this.monomials.get(i);
			ta = tmp.getA();
			if(ta != 0)
			{
				if(ta > 0)
					ret += "+";
				ret += tmp;
			}	
		}
		return ret;
	}
}



