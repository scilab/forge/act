/*	PNome.java
 *
 ** Authors :
 *
 *	Adrien KERFOURN
 *	Alexandre ŒCONOMOS
 *	Rémi THÉBAULT
 *
 ** License :
 *
 *	CeCILL v2
 *
 *	See (in english) :
 *	
 *		* Licence_CeCILL_V2-en.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 *
 *	See (in french) :
 *
 *		* Licence_CeCILL_V2-fr.txt
 *		* http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html
 *
 ** Description :
 *
 *	Allows to define a monomial of a polynomial.
 */

package Polynomial;

/** Allows to define a monomial of a polynomial.
 */
public class PNome implements Comparable<PNome>
{
	/** monomial coefficient. */
	private double a;
	/** monomial power. */	
	private int pow;

	/** character used to represent the polynomial variable. */
	private char var;
	
	/** Initialize a newly created PNome object.
	 *
	 * @param a monomial coefficient.
	 * @param pow monomial power.
	 * @param var polynomial variable.
	 */
	public PNome(double a, int pow, char var)
	{
		this.a = a;
		this.pow = pow;
		this.var = var;
	}

	/** Initialize a newly created PNome object with standart polynomial
	 * variable 'x'.
	 *
	 * @param a monomial coefficient.
	 * @param pow monomial power.
	 */
	public PNome(double a, int pow)
	{
		this(a, pow, 'x');
	}

	/** Initialize a newly created PNome object with null value (0*x^0).
	 *
	 */
	public PNome()
	{
		this(0.0,0);
	}

	/** Initialize a newly created PNome object with a string containing
	 * the monomial representation.
	 *
	 * @param nome string containing the monomial representation to be parsed.
	 * @param var polynomial/monomial variable (must be used in nome).
	 *
	 * @throws PolynomialException if the string does not contain a parsable
	 *	monomial.
	 */
	public PNome(String nome, char var) throws PolynomialException
	{
		this();
		this.fromString(nome, var);
	}

	/** Initialize a newly created PNome object with a string containing
	 * the monomial representation. The monomial variable must be 'x'.
	 *
	 * @param nome string containing the monomial representation to be parsed.
	 *
	 * @throws PolynomialException if the string does not contain a parsable
	 *	monomial.
	 */
	public PNome(String nome) throws PolynomialException
	{
		this();
		this.fromString(nome);
	}

	/** Gets the monomial coefficient.
	 *
	 * @return monomial coefficient.
	 */
	public double getA()
	{
		return this.a;
	}

	/** Gets the monomial power.
	 *
	 * @return monomial power.
	 */
	public int getPow()
	{
		return this.pow;
	}

	/** Sets the monomial coefficient.
	 *
	 * @param a monomial coefficient.
	 */
	public void setA(double a)
	{
		this.a = a;
	}

	/** Sets the monomial power.
	 *
	 * @param pow monomial power.
	 */
	public void setPow(int pow)
	{
		this.pow = pow;
	}

	/** Parse the string argument as monomial (in the current object)
	 * with 'x' as monomial variable. The string must only contain a
	 * monomial.
	 *
	 * @param nome : string containing the nome representation to be parsed.
	 */
	public void fromString(String nome) throws PolynomialException
	{
		this.fromString(nome,this.var);
	}

	/* Parse the string argument as monomial (in the current object).
	 * The string must only contain a monomial. The variable may be, for
	 * example : x, y, p, s, z, ...
	 *
	 * @param nome : string containing the nome representation to be parsed.
	 * @param dvar : monomial variable.
	 */
	public void fromString(String nome, char dvar) throws PolynomialException
	{
		int l = nome.length();
		String val = "";
		String var = "";
		boolean mul = false;
		boolean invsign = false;
		if(l == 0)
		{
			// Erreur : La chaine est vide.
			throw new PolynomialException(PolynomialException.E_M_EMPTY,null);
		}
		for(int i = 0; i < l; i++)
		{
			if(nome.charAt(i) > 32)
			{
				if(nome.charAt(i) == '*')
					mul = true;
				else
				{
					if(mul)
						var += nome.charAt(i);
					else
						val += nome.charAt(i);
				}
			}
		}
		if( (var.length() == 0) && (val.length() == 0))
		{
			// Erreur : La chaine est vide.
			throw new PolynomialException(PolynomialException.E_M_EMPTY,null);
		}

		if( (val.charAt(0) == '+') || (val.charAt(0) == '-') )
		{
			if(val.charAt(0) == '-')
				invsign = true;
			val = val.substring(1);
		}

		// Si la première partie correspond à la variable : on permute
		if(val.charAt(0) == dvar)
		{
			String tmp = new String(var);
			var = new String(val);
			val = new String(tmp);
		}

		// Si il y a une valeur de définie
		if(val.length() > 0)
		{
			try
			{
				this.a = Double.parseDouble(val);
			}
			catch(NumberFormatException e)
			{
				// Erreur lors de l'analyse d'une valeur d'un monôme
				throw new PolynomialException(PolynomialException.E_M_PARSE_N, val, e);
			}
		}
		else
		{
			this.a = 1.0;
		}

		// On traite les cas particuliers (pas de var et var seul)
		if(var.length() < 2)
		{
			this.pow = var.length(); // Hophophop : Si pas de var = 0, si juste var = 1
		}
		// Cas général :
		else
		{
			if(var.charAt(1) == '^')
			{
				l = var.length();
				String tmp = "";
				boolean ps = false;
				boolean pe = false;
				for(int i = 2; i<l ; i++)
				{
					tmp += var.charAt(i);
				}
				l = tmp.length();
				if(tmp.charAt(0) == '(')
					ps = true;
				if(tmp.charAt(l-1) == ')')
					pe = true;
				if(ps != pe)
				{
					//Il manque une parenthèse.
					throw new PolynomialException(PolynomialException.E_M_BRACKET,tmp);
				}
				if(ps)
					tmp = tmp.substring(1,l-1);

				try
				{
					this.pow = Integer.parseInt(tmp);
				}
				catch(NumberFormatException e)
				{
					// Erreur lors de l'analyse d'une valeur d'un monôme
					throw new PolynomialException(PolynomialException.E_M_PARSE_N, tmp, e);
				}
				
			}
			else
			{
				// Le symbole '^' est attendu après la variable.
				throw new PolynomialException(PolynomialException.E_M_POWNEED,var);
			}
		}
		this.var = dvar;
		if(invsign)
			this.a = -this.a;
	}

	/** Compare monomial powers between the current object and an other.
	 *
	 * @param obj Object compared to the current object.
	 *
	 * @return return 1 if the current object power if greater than the
	 * 	obj object power, -1 if the current object power if smaller than
	 *	the obj object power and else 0.
	 */
	public int compareTo(PNome obj)
	{
		if(this.pow > obj.getPow())
			return 1;
		if(this.pow < obj.getPow())
			return -1;
		return 0;
	}

	/** Returns a string object representing this monomial with an user
	 * defined variable.
	 *
	 * @param var variable used for the representation.
	 *
	 * @return a string representation of the value of this monomial.
	 */
	public String toString(char var)
	{
		String ret = "";
		if (this.a != 0)  // Si élément non nul
		{
			ret += this.a;
			if (this.pow != 0) // Si on à une puissance non nulle
			{
				ret += "*" + var;
				if (this.pow != 1) // Si on à une puissance 1, on affiche pas la puissance
				{
					ret += "^";
					if (this.pow > 0)
						ret += this.pow;
					else	 // Si la puissance est négative, on la met entre parenthèse
						ret += "(" + this.pow + ")";
				}
			}
		}
		return ret;
	}

	/** Returns a string object representing this monomial with the variable
	 * defined in the monomial.
	 *
	 * @return a string representation of the value of this monomial.
	 */
	public String toString()
	{
		return this.toString(this.var);
	}

}



