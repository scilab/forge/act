
import Polynomial.PNome;
import Polynomial.PolynomialException;


public class tstPNome
{
	public static void main(String [] args)
	{
		try
		{
		PNome p = new PNome(2,3);

		System.out.println(new PNome(2,3));
		System.out.println(new PNome(-1,3));
		System.out.println(new PNome(4,-2));
		System.out.println(new PNome(1,1));
		System.out.println(new PNome(-2,0));
		System.out.print("\n");

		System.out.println(new PNome("2*x^5"));
		System.out.println(new PNome("8*x^-1"));
		System.out.println(new PNome("x^(4)*2"));
		System.out.println(new PNome("3*x^1"));
		System.out.println(new PNome("x"));
		System.out.println(new PNome("1*x^(0)"));
		System.out.println(new PNome("x^(-8)*5"));
		System.out.println(new PNome("x^-3*2"));
		System.out.println(new PNome("2"));
		System.out.println(new PNome("0*x"));
		System.out.println(new PNome("-x*-2"));
		System.out.print("\n");

		System.out.println((new PNome("-2*z^-1",'z')).toString('z'));
		System.out.println((new PNome("+7*p^2",'p')).toString('p'));
		System.out.print("\n");

		System.out.println(new PNome("2*x(-2)"));
		}
		catch(PolynomialException e)
		{
			e.printStackTrace();
		}
	}

}


